package dawid.brelak.greensunserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableScheduling
@EnableAsync
@EnableJpaRepositories(repositoryImplementationPostfix = "Seed")
public class GreenSunServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenSunServerApplication.class, args);
	}
	@Bean
	public RestTemplate restTemplate() {
		int duration=10000;
		RestTemplate restTemplate = new RestTemplate();
		SimpleClientHttpRequestFactory factory= (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
		factory.setConnectTimeout(duration);
	//	restTemplate.setRequestFactory(factory);
	    return restTemplate;
	}
}
