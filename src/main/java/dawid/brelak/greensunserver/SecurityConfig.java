package dawid.brelak.greensunserver;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/godlikePanel","/godlikePanel/*","/godlikePanel/*/*","/godlikePanel/*/*/*","/godlikePanel/*/*/*/*","/godlikePanel/*/*/*/*/*")
		.hasRole("GodlikeAdmin").antMatchers("*").permitAll()
		.and().formLogin().loginPage("/login")
		.and().logout().logoutUrl("/logout").logoutSuccessUrl("/").invalidateHttpSession(true).deleteCookies("JSESSIONID");
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		auth.inMemoryAuthentication().passwordEncoder(encoder)
			.withUser("godlike").password(encoder.encode("godlike"))
				.roles("GodlikeAdmin").and().withUser("smarthome").password(encoder.encode("smarthome")).roles("GodlikeAdmin");
	}

}
