package dawid.brelak.greensunserver.connectors;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;



/**
 * Rest connector dedicated to connect with device powered by ARest Arduino library
 * @author Dawid
 * @since 2018-11-15
 * @version 1
 */
@Service
public class ARestConnector {
	@Autowired
	private RestTemplate restTemplate;

	public ARestConnector() {

	}
	public ARestConnector(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}
	/**
	 * Return JSON response as String
	 * @param url
	 * @return
	 */
	public String getAsString(String url) {
		try {
			ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
			return result.getBody();
		} catch (Exception e) {
			return null;
		}

	}
	/**
	 * Parse response to selected class or return null  
	 * @param url
	 * @param type
	 * @return
	 */
	public <T> T getAsObject(String url, Class<T> type) {
		try {
			ResponseEntity<T> result = restTemplate.getForEntity(url, type);
			return result.getBody();
		} catch (Exception e) {
			return null;
		}

	}
	/**
	 * Cut String JSON response and return part list that first element is key, second is value and so on
	 * @param jsonResponse
	 * @return
	 */
	public List<String> splitResponse(String jsonResponse) {
		try {
			jsonResponse = jsonResponse.replace("{", "");
			jsonResponse = jsonResponse.replace("}", "");
			jsonResponse = jsonResponse.replace("\"", "");

			String[] parts = jsonResponse.split(":|,");
			ArrayList<String> list = new ArrayList<>();
			for (String part : parts) {
				list.add(part.trim());
			}
			return list;
		} catch (Exception e) {
			return null;
		}

	}

}
