package dawid.brelak.greensunserver.connectors;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;

/**
 * Open and load {@link Device} configuration from file
 * @author Dawid
 * @since 2018-12-23
 * @version 1
 *
 */
@Service
public class DeviceConfigFileConnector {
	/**
	 * Open file in given location and return {@link Device}
	 * @param filePath
	 * @return
	 */
	public Device loadFromCSVFile(Path filePath) {
		Device device = null;
		List<DeviceResource> resources = new ArrayList<DeviceResource>();
		try {
			Reader csvReader = Files.newBufferedReader(filePath,Charset.forName("UTF-8"));
			CSVParser csvParser = new CSVParser(csvReader, CSVFormat.DEFAULT.withDelimiter(';').withTrim());
			List<CSVRecord> recordsList = csvParser.getRecords();
			// Get headers and data rows
			for (int recordIndex = 0; recordIndex < recordsList.size(); recordIndex += 2) {
				CSVRecord csvHeaderRecord = recordsList.get(recordIndex);
				CSVRecord csvRecord = recordsList.get(recordIndex + 1);
				if (csvHeaderRecord.get(0).toLowerCase().equals("#resource".toLowerCase())) {
					// Transform record to device resource
					if (csvRecord.get(0).toLowerCase().equals("BooleanResource".toLowerCase())) {
						BooleanResource resource = bulidBooleanResource(csvHeaderRecord, csvRecord);
						resource.setDevice(device);
						resources.add(resource);
					} else if (csvRecord.get(0).toLowerCase().equals("CharacterResource".toLowerCase())) {
						CharacterResource resource =bulidCharacterResource(csvHeaderRecord, csvRecord);
						resource.setDevice(device);
						resources.add(resource);
					} else if (csvRecord.get(0).toLowerCase().equals("IntegerResource".toLowerCase())) {
						IntegerResource resource =bulidIntegerResource(csvHeaderRecord, csvRecord);
						resource.setDevice(device);
						resources.add(resource);
					} else if (csvRecord.get(0).toLowerCase().equals("FloatResource".toLowerCase())) {
						FloatResource resource =bulidFloatResource(csvHeaderRecord, csvRecord);
						resource.setDevice(device);
						resources.add(resource);
					}
					// Bulid devices
				} else if (csvHeaderRecord.get(0).toLowerCase().equals("#device".toLowerCase())) {
					if (csvRecord.get(0).toLowerCase().equals("ARestDevice".toLowerCase())) {
						device = bulidARestDevice(csvHeaderRecord, csvRecord);
					}
				}
			}
			csvParser.close();
			csvReader.close();
			device.setResources(resources);
			return device;
		} catch (Exception e) {
//			e.printStackTrace();
			return null;			
		}
	}
	/**
	 * Write configuration file to selected file
	 * @param device
	 * @param filePath
	 */
	public void exportToFile(Device device, Path filePath) {
		try {
			BufferedWriter writer = Files.newBufferedWriter(filePath,Charset.forName("UTF-8"));
			CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withDelimiter(';'));
			if (device instanceof RestDevice) {
				csvPrinter.printRecord("#device", "Name", "IP", "Description");
				csvPrinter.printRecord("ARestDevice", device.getName(), ((RestDevice) device).getAddress(),
						device.getDescription());
			}
			for (DeviceResource deviceResource : device.getResources()) {
				List<String> keys = new ArrayList<>();
				List<String> values = new ArrayList<>();
				////
				keys.add("name");
				values.add(deviceResource.getName());
				keys.add("device_resource");
				values.add(deviceResource.getResource());
				keys.add("read_only");
				values.add(deviceResource.getReadOnly() ? "yes" : "no");
				keys.add(0, "#resource");
				////
				if (deviceResource instanceof BooleanResource) {
					values.add(0, "BooleanResource");
					keys.add("off_label");
					values.add(((BooleanResource) deviceResource).getOffLabel());
					keys.add("on_label");
					values.add(((BooleanResource) deviceResource).getOnLabel());
				} else if (deviceResource instanceof IntegerResource) {
					values.add(0, "IntegerResource");
					keys.add("min_value");
					values.add(((IntegerResource) deviceResource).getMinValue().toString());
					keys.add("max_value");
					values.add(((IntegerResource) deviceResource).getMaxValue().toString());					
				} else if (deviceResource instanceof FloatResource) {
					values.add(0, "FloatResource");
					keys.add("min_value");
					values.add(((FloatResource) deviceResource).getMinValue().toString());
					keys.add("max_value");
					values.add(((FloatResource) deviceResource).getMaxValue().toString());
				} else if (deviceResource instanceof CharacterResource) {
					values.add(0, "CharacterResource");
					keys.add("length");
					values.add(((CharacterResource) deviceResource).getMaxLength().toString());
				}
				////
				csvPrinter.printRecord(keys);
				csvPrinter.printRecord(values);
			}

			csvPrinter.flush();
			csvPrinter.close();
			writer.close();
		} catch (IOException e) {
		
//			e.printStackTrace();
		}

	}
	
	private RestDevice bulidARestDevice(CSVRecord headerRecord, CSVRecord dataRecord) {
		if (headerRecord.size() == dataRecord.size()) {
			RestDevice restDevice = new RestDevice();
			for (int i = 0; i < headerRecord.size(); i++) {
				if (headerRecord.get(i).toLowerCase().equals("name".toLowerCase())) {
					restDevice.setName(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("ip".toLowerCase())) {
					restDevice.setAddress(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("description".toLowerCase())) {
					restDevice.setDescription(dataRecord.get(i));
				}
			}
			return restDevice;
		} else {
			return null;
		}

	}

	private BooleanResource bulidBooleanResource(CSVRecord headerRecord, CSVRecord dataRecord) {
		if (headerRecord.size() == dataRecord.size()) {
			BooleanResource resource = new BooleanResource();
			for (int i = 0; i < headerRecord.size(); i++) {
				if (headerRecord.get(i).toLowerCase().equals("name".toLowerCase())) {
					resource.setName(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("device_resource".toLowerCase())) {
					resource.setResource(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("read_only".toLowerCase())) {
					resource.setReadOnly(dataRecord.get(i).toLowerCase().equals("yes"));
				} else if (headerRecord.get(i).toLowerCase().equals("off_label".toLowerCase())) {
					resource.setOffLabel(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("on_label".toLowerCase())) {
					resource.setOnLabel(dataRecord.get(i));
				}
			}
			return resource;
		} else {
			return null;
		}
	}

	private CharacterResource bulidCharacterResource(CSVRecord headerRecord, CSVRecord dataRecord) {
		if (headerRecord.size() == dataRecord.size()) {
			CharacterResource resource = new CharacterResource();
			for (int i = 0; i < headerRecord.size(); i++) {
				if (headerRecord.get(i).toLowerCase().equals("name".toLowerCase())) {
					resource.setName(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("device_resource".toLowerCase())) {
					resource.setResource(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("read_only".toLowerCase())) {
					resource.setReadOnly(dataRecord.get(i).toLowerCase().equals("yes"));
				} else if (headerRecord.get(i).toLowerCase().equals("length".toLowerCase())) {
					resource.setMaxLength(Integer.parseInt(dataRecord.get(i)));
				}
			}
			return resource;
		} else {
			return null;
		}
	}

	private IntegerResource bulidIntegerResource(CSVRecord headerRecord, CSVRecord dataRecord) {
		if (headerRecord.size() == dataRecord.size()) {
			IntegerResource resource = new IntegerResource();
			for (int i = 0; i < headerRecord.size(); i++) {
				if (headerRecord.get(i).toLowerCase().equals("name".toLowerCase())) {
					resource.setName(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("device_resource".toLowerCase())) {
					resource.setResource(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("read_only".toLowerCase())) {
					resource.setReadOnly(dataRecord.get(i).toLowerCase().equals("yes"));
				} else if (headerRecord.get(i).toLowerCase().equals("min_value".toLowerCase())) {
					resource.setMinValue(Integer.parseInt(dataRecord.get(i)));
				} else if (headerRecord.get(i).toLowerCase().equals("max_value".toLowerCase())) {
					resource.setMaxValue(Integer.parseInt(dataRecord.get(i)));
				}
			}
			return resource;
		} else {
			return null;
		}
	}

	private FloatResource bulidFloatResource(CSVRecord headerRecord, CSVRecord dataRecord) {
		if (headerRecord.size() == dataRecord.size()) {
			FloatResource resource = new FloatResource();
			for (int i = 0; i < headerRecord.size(); i++) {
				if (headerRecord.get(i).toLowerCase().equals("name".toLowerCase())) {
					resource.setName(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("device_resource".toLowerCase())) {
					resource.setResource(dataRecord.get(i));
				} else if (headerRecord.get(i).toLowerCase().equals("read_only".toLowerCase())) {
					resource.setReadOnly(dataRecord.get(i).toLowerCase().equals("yes"));
				} else if (headerRecord.get(i).toLowerCase().equals("min_value".toLowerCase())) {
					resource.setMinValue(Float.parseFloat(dataRecord.get(i)));
				} else if (headerRecord.get(i).toLowerCase().equals("max_value".toLowerCase())) {
					resource.setMaxValue(Float.parseFloat(dataRecord.get(i)));
				}
			}
			return resource;
		} else {
			return null;
		}
	}

}
