package dawid.brelak.greensunserver.controllers.admin.pages;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import dawid.brelak.greensunserver.connectors.DeviceConfigFileConnector;
import dawid.brelak.greensunserver.model.analysis.Event;
import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;
import dawid.brelak.greensunserver.repositories.DeviceResourceRepository;
import dawid.brelak.greensunserver.repositories.EventRepository;
import dawid.brelak.greensunserver.repositories.MontlyUsageRepository;
import dawid.brelak.greensunserver.repositories.RestDeviceRepository;
import dawid.brelak.greensunserver.services.ARestService;
import dawid.brelak.greensunserver.services.DeviceAutoReadService;

/**
 * Administration panel controller used for management {@link Device}
 * 
 * @author Dawid
 * @version 1.0
 * @since 2018-11-02
 *
 */
@Controller
@RequestMapping("/godlikePanel")
public class AdminPageDeviceController {
	@Autowired
	private RestDeviceRepository deviceRepository;
	@Autowired
	private DeviceResourceRepository resourceRepository;
	@Autowired
	private ARestService aRestService;
	@Autowired
	private EventRepository eventRepository;
	@Autowired
	private MontlyUsageRepository montlyUsageRepository;
	@Autowired
	private DeviceConfigFileConnector fileConfigLoader;
	@Autowired
	private DeviceAutoReadService readService;

	Random random = new Random();

	/**
	 * Return admin panel main template
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping
	public String godlikePanel(Model model) {
		return "godlikePanel/godlikePanel";
	}

	//// Rest Api Devices
	/**
	 * Display List {@link Device} controlled by ARest library
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/restDevices")
	public String restDevice(Model model) {
		model.addAttribute("restDevices", deviceRepository.findAll());
		return "godlikePanel/restDevice/restDevices";
	}

	/**
	 * Delete {@link RestDevice} using pathVariable
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/deleteRestDevice/{id}")
	public String deleteRestDevice(@PathVariable Integer id, Model model) {
		try {
			RestDevice device = deviceRepository.findById(id).get();
			if(device.getResources()!=null&&device.getResources().size()>0) {
			List<Integer> list =device.getResources().stream().map(DeviceResource::getId).collect(Collectors.toList());
			montlyUsageRepository.removeByResourcesIds(list);
			montlyUsageRepository.notifyDataChange();
			//TODO not removed resources
			readService.removeResource(list);
			}
			Event event = new Event();
			event.setEventDate(LocalDateTime.now());
			event.setEventInfo("Remove device");
			event.setEventObject(device.getName());
			event.setEventOwner("AdminPage");
			deviceRepository.delete(device);
			deviceRepository.notifyDataChange();
			eventRepository.save(event);
		} catch (Exception e) {
		}
		return "redirect:/godlikePanel/restDevices";
	}

	/**
	 * Return template with new {@link RestDevice} form
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/newRestDevice")
	public String newRestDevice(Model model) {
		model.addAttribute("restDevice", new RestDevice());
		return "godlikePanel/restDevice/newRestDevice";
	}
	/**
	 * Edit device template
	 * @param model
	 * @param id
	 * @return
	 */
	@GetMapping("/editRestDevice/{id}")
	public String editRestDevice(Model model,@PathVariable Integer id) {
		RestDevice device = deviceRepository.findById(id).orElse(new RestDevice());
		model.addAttribute("restDevice", device);
		return "godlikePanel/restDevice/newRestDevice";
	}

	/**
	 * Save {@link RestDevice} submitted in form and redirect to list page
	 * 
	 * @param restDevice
	 * @return
	 */
	@PostMapping("/newRestDevice")
	public String saveNewRestDevice(RestDevice restDevice) {
		deviceRepository.save(restDevice);
		deviceRepository.notifyDataChange();
		montlyUsageRepository.notifyDataChange();
		Event event = new Event();
		event.setEventDate(LocalDateTime.now());
		event.setEventInfo(restDevice.getId()==null?"New device":"Device edit");
		event.setEventObject(restDevice.getName());
		event.setEventOwner("AdminPage");
		eventRepository.save(event);
		return "redirect:/godlikePanel/restDevices";
	}

	/**
	 * Load {@link RestDevice} details page
	 * 
	 * @param model
	 * @param deviceId
	 * @return
	 */
	@GetMapping("/deviceDetails/{deviceId}")
	public String deviceDetails(Model model, @PathVariable Integer deviceId) {
		RestDevice restDevice = deviceRepository.findById(deviceId).orElse(null);
		model.addAttribute("deviceDetails", restDevice);
		return "godlikePanel/restDevice/restDeviceDetails";
	}

	/**
	 * Load {@link DeviceResource} selected by path variable and return form
	 * template
	 * 
	 * @param model
	 * @param resourceType
	 * @param deviceId
	 * @return
	 */
	@GetMapping("/deviceResource/{resourceType}/{deviceId}")
	public String deviceResource(Model model, @PathVariable String resourceType, @PathVariable Integer deviceId) {
		DeviceResource deviceResource = resourceRepository.newByName(resourceType);
		RestDevice restDevice = deviceRepository.findById(deviceId).orElse(null);
		deviceResource.setDevice(restDevice);
		model.addAttribute("resource", deviceResource);
		return "godlikePanel/restDevice/newRestDeviceResource";
	}
	/**
	 * {@link DeviceResource} edit template
	 * @param model
	 * @param resourceId
	 * @return
	 */
	@GetMapping("/editDeviceResource/{resourceId}")
	public String editDeviceResource(Model model, @PathVariable Integer resourceId) {
		DeviceResource deviceResource = resourceRepository.findById(resourceId).orElse(null);
		model.addAttribute("resource", deviceResource);
		return "godlikePanel/restDevice/newRestDeviceResource";
	}

	/**
	 * Process and save {@link CharacterResource}
	 * 
	 * @param model
	 * @param deviceResource
	 * @param deviceId
	 * @return
	 */
	@PostMapping("/deviceResource/characterResource/{deviceId}")
	public String deviceResource(Model model, CharacterResource deviceResource, @PathVariable Integer deviceId) {
		RestDevice device = deviceRepository.findById(deviceId).orElse(null);
		deviceResource.setDevice(device);
		if(deviceResource.getId()!=null)
			readService.replaceResource(deviceResource);
		resourceRepository.save(deviceResource);
		deviceRepository.notifyDataChange();
		return "redirect:/godlikePanel/deviceDetails/" + deviceId;
	}

	/**
	 * Process and save {@link CharacterSwitchResource}
	 * 
	 * @param model
	 * @param deviceResource
	 * @param deviceId
	 * @return
	 */
	@PostMapping("/deviceResource/booleanResource/{deviceId}")
	public String deviceResource(Model model, BooleanResource deviceResource, @PathVariable Integer deviceId) {
		RestDevice device = deviceRepository.findById(deviceId).orElse(null);
		deviceResource.setDevice(device);
		if(deviceResource.getId()!=null)
			readService.replaceResource(deviceResource);
		resourceRepository.save(deviceResource);
		deviceRepository.notifyDataChange();
		return "redirect:/godlikePanel/deviceDetails/" + deviceId;
	}

	/**
	 * Process and save {@link IntegerResource}
	 * 
	 * @param model
	 * @param deviceResource
	 * @param deviceId
	 * @return
	 */
	@PostMapping("/deviceResource/integerResource/{deviceId}")
	public String deviceResource(Model model, IntegerResource deviceResource, @PathVariable Integer deviceId) {
		RestDevice device = deviceRepository.findById(deviceId).orElse(null);
		deviceResource.setDevice(device);
		if(deviceResource.getId()!=null)
			readService.replaceResource(deviceResource);
		resourceRepository.save(deviceResource);
		deviceRepository.notifyDataChange();
		return "redirect:/godlikePanel/deviceDetails/" + deviceId;
	}

	/**
	 * Process and save {@link FloatResource}
	 * 
	 * @param model
	 * @param deviceResource
	 * @param deviceId
	 * @return
	 */
	@PostMapping("/deviceResource/floatResource/{deviceId}")
	public String deviceResource(Model model, FloatResource deviceResource, @PathVariable Integer deviceId) {
		RestDevice device = deviceRepository.findById(deviceId).orElse(null);
		deviceResource.setDevice(device);
		if(deviceResource.getId()!=null)
			readService.replaceResource(deviceResource);
		resourceRepository.save(deviceResource);
		deviceRepository.notifyDataChange();
		return "redirect:/godlikePanel/deviceDetails/" + deviceId;
	}

	/**
	 * Process and remove {@link DeviceResource}
	 * 
	 * @param deviceId
	 * @param resourceId
	 * @param model
	 * @return
	 */
	@GetMapping("/deleteDeviceResource/{deviceId}/{resourceId}")
	public String deleteDeviceResource(@PathVariable Integer deviceId, @PathVariable Integer resourceId, Model model) {
		try {
			RestDevice restDevice = deviceRepository.findById(deviceId).get();
			DeviceResource deviceResource = restDevice.getResources().stream().filter(d -> d.getId().equals(resourceId))
					.findAny().get();
			restDevice.getResources().remove(deviceResource);
			deviceResource.setDevice(null);
			resourceRepository.delete(deviceResource);
			deviceRepository.notifyDataChange();
		} catch (Exception e) {
		}
		return "redirect:/godlikePanel/deviceDetails/" + deviceId;
	}

	//// Connection test
	/**
	 * Returns template for list of registered {@link Device}
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/connectionTest")
	public String connectionTest(Model model) {
		model.addAttribute("deviceResources", resourceRepository.findAll());
		return "godlikePanel/connectionTest/testConnection";
	}

	/**
	 * Randomly set value for connection test, then return template
	 * 
	 * @param model
	 * @param resourceId
	 * @return
	 */
	@GetMapping("/testDetails/{resourceId}")
	public String testDetails(Model model, @PathVariable Integer resourceId) {
		DeviceResource deviceResource = resourceRepository.findById(resourceId).orElse(null);
		if (deviceResource instanceof CharacterResource) {
			deviceResource.setCurrentState("");
		} else if (deviceResource instanceof BooleanResource) {
			deviceResource.setCurrentState(((BooleanResource) deviceResource).getOffLabel());
		} else if (deviceResource instanceof IntegerResource) {
			Integer min = ((IntegerResource) deviceResource).getMinValue();
			Integer max = ((IntegerResource) deviceResource).getMaxValue();
			Integer value = random.ints(min, max).findFirst().getAsInt();
			deviceResource.setCurrentState(value);
		} else if (deviceResource instanceof FloatResource) {
			Float min = ((FloatResource) deviceResource).getMinValue();
			Float max = ((FloatResource) deviceResource).getMaxValue();
			Float value = (float) random.doubles(min, max).findFirst().getAsDouble();
			String svalue = Float.toString(value);
			svalue = svalue.substring(0, svalue.indexOf('.') + 3);
			deviceResource.setCurrentState(Float.parseFloat(svalue));
		}
		model.addAttribute("resource", deviceResource);
		return "godlikePanel/connectionTest/testDetails";
	}

	/**
	 * Using {@link ARestService} try to connect with {@link FloatingPointResource}
	 * then forward to summary
	 * 
	 * @param model
	 * @param resource
	 * @param deviceId
	 * @param request
	 * @return
	 */
	@PostMapping("/testDetails/float/{deviceId}")
	public String testDetails(Model model, FloatResource resource, @PathVariable Integer deviceId,
			@RequestParam("currentState") Object currentState, HttpServletRequest request) {
		Device device = deviceRepository.findById(deviceId).orElse(null);
		resource.setDevice(device);

		if (resource.getReadOnly()) {
			resource = aRestService.readResource(resource);
		} else {
			Float value = Float.parseFloat((String) currentState);
			resource.setCurrentState(value);
			resource = aRestService.writeResource(resource, (float) resource.getCurrentState());
		}
		request.setAttribute("resource", resource);
		return "forward:/godlikePanel/testSummary/float";
	}

	/**
	 * Using {@link ARestService} try to connect with {@link IntegerResource} then
	 * forward to summary
	 * 
	 * @param resource
	 * @param deviceId
	 * @param currentState
	 * @param request
	 * @return
	 */
	@PostMapping("/testDetails/integer/{deviceId}")
	public String testDetails(IntegerResource resource, @PathVariable Integer deviceId,
			@RequestParam("currentState") Object currentState, HttpServletRequest request) {
		Device device = deviceRepository.findById(deviceId).orElse(null);
		resource.setDevice(device);
		if (resource.getReadOnly()) {
			resource = aRestService.readResource(resource);
		} else {

			Integer value = Integer.parseInt((String) currentState);
			resource.setCurrentState(value);
			resource = aRestService.writeResource(resource, (int) resource.getCurrentState());

		}
		request.setAttribute("resource", resource);
		return "forward:/godlikePanel/testSummary/integer";
	}

	/**
	 * Using {@link ARestService} try to connect with
	 * {@link CharacterSwitchResource} then forward to summary
	 * 
	 * @param resource
	 * @param deviceId
	 * @param currentState
	 * @param request
	 * @return
	 */
	@PostMapping("/testDetails/boolean/{deviceId}")
	public String testDetails(BooleanResource resource, @PathVariable Integer deviceId,
			@RequestParam("currentState") Object currentState, HttpServletRequest request) {
		Device device = deviceRepository.findById(deviceId).orElse(null);
		resource.setDevice(device);
		if (resource.getReadOnly()) {
			resource = aRestService.readResource(resource);
		} else {
			resource.setCurrentState((String) currentState);
			resource = aRestService.writeResource(resource, (String) resource.getCurrentState());

		}
		request.setAttribute("resource", resource);
		return "forward:/godlikePanel/testSummary/boolean";
	}

	/**
	 * Using {@link ARestService} try to connect with {@link CharacterResource} then
	 * forward to summary
	 * 
	 * @param resource
	 * @param deviceId
	 * @param currentState
	 * @param request
	 * @return
	 */
	@PostMapping("/testDetails/character/{deviceId}")
	public String testDetails(CharacterResource resource, @PathVariable Integer deviceId,
			@RequestParam("currentState") Object currentState, HttpServletRequest request) {
		Device device = deviceRepository.findById(deviceId).orElse(null);
		resource.setDevice(device);
		if (resource.getReadOnly()) {
			resource = aRestService.readResource(resource);
		} else {
			resource.setCurrentState((String) currentState);
			resource = aRestService.writeResource(resource, (String) resource.getCurrentState());
		}
		request.setAttribute("resource", resource);
		return "forward:/godlikePanel/testSummary/character";
	}

	/**
	 * Load template summary for test connection with {@link FloatingPointResource}
	 * 
	 * @param resource
	 * @param model
	 * @return
	 */
	@PostMapping("/testSummary/float")
	public String testSummary(@RequestAttribute("resource") FloatResource resource, Model model) {
		model.addAttribute("resource", resource);
		return "godlikePanel/connectionTest/testSummary";
	}

	/**
	 * Load template summary for test connection with {@link IntegerResource}
	 * 
	 * @param resource
	 * @param model
	 * @return
	 */
	@PostMapping("/testSummary/integer")
	public String testSummary(@RequestAttribute("resource") IntegerResource resource, Model model) {
		model.addAttribute("resource", resource);
		return "godlikePanel/connectionTest/testSummary";
	}

	/**
	 * Load template summary for test connection with
	 * {@link CharacterSwitchResource}
	 * 
	 * @param resource
	 * @param model
	 * @return
	 */
	@PostMapping("/testSummary/boolean")
	public String testSummary(@RequestAttribute("resource") BooleanResource resource, Model model) {
		model.addAttribute("resource", resource);
		return "godlikePanel/connectionTest/testSummary";
	}

	/**
	 * Load template summary for test connection with {@link CharacterResource}
	 * 
	 * @param resource
	 * @param model
	 * @return
	 */
	@PostMapping("/testSummary/character")
	public String testSummary(@RequestAttribute("resource") CharacterResource resource, Model model) {
		model.addAttribute("resource", resource);
		return "godlikePanel/connectionTest/testSummary";
	}

	//// Load config from file
	/**
	 * Configuration file selection template  
	 * @param model
	 * @return
	 */
	@GetMapping("/fileConfig")
	public String fileConfig(Model model) {
		return "godlikePanel/restDevice/loadDeviceFile";
	}
	/**
	 * Save submitted file on disk and set path for it
	 * @param multipartFile
	 * @param model
	 * @param request
	 * @return
	 */
	@PostMapping("/fileConfig")
	public String fileConfigPost(@RequestParam("file") MultipartFile multipartFile, Model model,
			HttpServletRequest request) {
		String file = "config_" + UUID.randomUUID().toString() + ".csv";
		try {
			File configFile = new File(file);
			FileOutputStream outputStream = new FileOutputStream(configFile);
			outputStream.write(multipartFile.getBytes());
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			System.err.println("Not working!!!");
			e.printStackTrace();
		}
		request.setAttribute("configPath", file);
		return "forward:/godlikePanel/fileConfig/Summary";
	}
	/**
	 * get saved file path and process and save it 
	 * @param model
	 * @param request
	 * @return
	 */
	@PostMapping("/fileConfig/Summary")
	public String fileConfigSummary(Model model, HttpServletRequest request) {
		Device device = null;
		String file = (String) request.getAttribute("configPath");
		device = fileConfigLoader.loadFromCSVFile(Paths.get(file));
		if (device != null) {
			try {
				deviceRepository.save((RestDevice) device);
				deviceRepository.notifyDataChange();
				montlyUsageRepository.notifyDataChange();
				Event event = new Event();
				event.setEventDate(LocalDateTime.now());
				event.setEventInfo("New device");
				event.setEventObject(device.getName());
				event.setEventOwner("AdminPage");
				eventRepository.save(event);
				model.addAttribute("device", device);
			} catch (Exception e) {
				model.addAttribute("errorStatus", "address");
				model.addAttribute("device", null);
			}
		} else {
			model.addAttribute("errorStatus", "file");
			model.addAttribute("device", null);
		}
		try {
			Files.deleteIfExists(Paths.get(file));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "godlikePanel/restDevice/loadDeviceSummary";
	}

}
