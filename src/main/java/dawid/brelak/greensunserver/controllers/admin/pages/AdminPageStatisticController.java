package dawid.brelak.greensunserver.controllers.admin.pages;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import dawid.brelak.greensunserver.model.analysis.Event;
import dawid.brelak.greensunserver.model.analysis.Event_;
import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsage;
import dawid.brelak.greensunserver.repositories.EventRepository;
import dawid.brelak.greensunserver.repositories.MontlyUsageRepository;

/**
 * Administration panel controller correspond to system statistic
 * 
 * @author Dawid
 * @version 1.0
 * @since 2018-11-02
 *
 */
@Controller
@RequestMapping("/godlikePanel")
public class AdminPageStatisticController {
	@Autowired
	private EventRepository eventRepository;
	@Autowired
	private MontlyUsageRepository montlyUsageRepository;
	/**
	 * Load {@link Event} template
	 * @param model
	 * @return
	 */
	@GetMapping("/events")
	public String eventsPage(Model model) {
		model.addAttribute("eventsList", eventRepository.findAll(new Sort(Direction.DESC, Event_.EVENT_DATE)));
		return "godlikePanel/statistics/events";
	}
	/**
	 * Load {@link MonthlyResourceUsage} using current year and month
	 * @param model
	 * @return
	 */
	@GetMapping("/monthlyUsage")
	public String monthlyUsagePage(Model model) {
		LocalDate localDate =LocalDate.now();
		model.addAttribute("usageList", montlyUsageRepository.findAllByYearAndMonth(localDate.getYear(), localDate.getMonthValue()));
		return "godlikePanel/statistics/monthlyUsage";
	}

}
