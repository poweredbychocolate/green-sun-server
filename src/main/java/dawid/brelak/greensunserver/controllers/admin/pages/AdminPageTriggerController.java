package dawid.brelak.greensunserver.controllers.admin.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.model.trigger.ResourceTrigger;
import dawid.brelak.greensunserver.model.trigger.StandardTrigger;
import dawid.brelak.greensunserver.model.trigger.TriggerAction;
import dawid.brelak.greensunserver.repositories.DeviceResourceRepository;
import dawid.brelak.greensunserver.repositories.TriggerRepository;

/**
 * Administration panel controller for {@link ResourceTrigger}
 * 
 * @author Dawid
 * @version 1
 * @since 2018-11-27
 *
 */
@Controller
@RequestMapping("/godlikePanel")
public class AdminPageTriggerController {
	@Autowired
	private TriggerRepository triggerRepository;
	@Autowired
	private DeviceResourceRepository resourceRepository;
	@Autowired
	private MessageSource messageSource;

	/**
	 * {@link ResourceTrigger} list
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/triggers")
	public String triggers(Model model) {
		model.addAttribute("triggers", triggerRepository.findAll());
		return "godlikePanel/trigger/triggers";
	}

	/**
	 * New {@link ResourceTrigger} template
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/newTrigger")
	public String newTrigger(Model model) {
		List<String> languageTypesList = new ArrayList<String>();
		List<String> typesList = new ArrayList<String>(StandardTrigger.getTypes());
		MessageSourceAccessor messageAccessor = new MessageSourceAccessor(messageSource);
		for (String type : StandardTrigger.getTypes()) {
			languageTypesList.add(messageAccessor.getMessage("type." + type));
		}

		TriggerResponse triggerResponse = new TriggerResponse();
		model.addAttribute("languageTypes", languageTypesList);
		model.addAttribute("types", typesList);
		triggerResponse.setValue(new String());
		model.addAttribute("resources", resourceRepository.findAll());
		model.addAttribute("triggerResponse", triggerResponse);
		return "godlikePanel/trigger/newTrigger";
	}

	/**
	 * {@link ResourceTrigger} type and source {@link DeviceResource}
	 * 
	 * @param model
	 * @param triggerResponse
	 * @return
	 */
	@PostMapping("/newTrigger")
	public String newTriggerPost(Model model, TriggerResponse triggerResponse) {
		System.err.println("Value = " + triggerResponse.value);
		System.err.println("Type = " + triggerResponse.type);
		System.err.println("Resource = " + triggerResponse.resourceId);
		model.addAttribute("resource", resourceRepository.findById(triggerResponse.getResourceId()).get());
		model.addAttribute("resources", resourceRepository.findAllByReadOnly(false));
		model.addAttribute("trigger", triggerResponse);
		model.addAttribute("savedTrigger", null);
		return "godlikePanel/trigger/newTriggerSummary";
	}

	/**
	 * Process and save configuration
	 * 
	 * @param model
	 * @param triggerResponse
	 * @return
	 */
	@PostMapping("/newTriggerFinalize")
	public String newTriggerFinalize(Model model, TriggerResponse triggerResponse) {
		System.err.println(triggerResponse);
		System.err.println("Trigger type = " + triggerResponse.type);
		System.err.println("Trigger id = " + triggerResponse.triggerId);
		System.err.println("Resource id = " + triggerResponse.resourceId);
		System.err.println("Resource Value = " + triggerResponse.value);
		System.err.println("Action Resource Id = " + triggerResponse.actionResourceId);
		System.err.println("Action Value = " + triggerResponse.actionValue);
		////
		ResourceTrigger resourceTrigger = triggerRepository
				.findById(triggerResponse.getTriggerId() != null ? triggerResponse.getTriggerId() : -1).orElse(null);
		if (resourceTrigger == null) {
			if (StandardTrigger.getTypes().contains(triggerResponse.getType())) {
				resourceTrigger = new StandardTrigger();
				((StandardTrigger) resourceTrigger).setTriggerType(triggerResponse.getType());
				DeviceResource resource = resourceRepository.findById(triggerResponse.getResourceId()).get();
				resourceTrigger.setSourceResource(resource);
				resourceTrigger.setSourceValue(triggerResponse.getValue());
			}
		}
		if (resourceTrigger != null) {
			DeviceResource resource = resourceRepository.findById(triggerResponse.getActionResourceId()).get();
			TriggerAction triggerAction = new TriggerAction();
			triggerAction.setResource(resource);
			triggerAction.setValue(triggerResponse.getActionValue());
			resourceTrigger.getTriggerActions().add(triggerAction);
			triggerRepository.save(resourceTrigger);
			triggerRepository.notifyDataChange();
			triggerResponse.setTriggerId(resourceTrigger.getId());
		}
		////
		ResourceTrigger savedTrigger = triggerRepository
				.findById(triggerResponse.getTriggerId() != null ? triggerResponse.getTriggerId() : -1).orElse(null);
		triggerResponse.setActionResourceId(null);
		triggerResponse.setActionValue(null);
		model.addAttribute("resource", resourceRepository.findById(triggerResponse.getResourceId()).get());
		model.addAttribute("resources", resourceRepository.findAllByReadOnly(false));
		model.addAttribute("trigger", triggerResponse);
		model.addAttribute("savedTrigger", savedTrigger);
		return "godlikePanel/trigger/newTriggerSummary";
	}

	/**
	 * Delete {@link ResourceTrigger} using id
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/deleteTrigger/{id}")
	public String deleteTrigger(@PathVariable Long id, Model model) {
		ResourceTrigger trigger = triggerRepository.findById(id).orElse(null);
		try {
			if (trigger != null) {
				triggerRepository.delete(trigger);
				triggerRepository.notifyDataChange();
			}
		} catch (Exception e) {
			try {
				if (trigger != null) {
					List<Long> list = trigger.getTriggerActions().stream().map(TriggerAction::getId)
							.collect(Collectors.toList());
					trigger.getTriggerActions().clear();
					triggerRepository.delete(trigger);
					triggerRepository.removeActions(list);
					triggerRepository.notifyDataChange();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}

		}
		return "redirect:/godlikePanel/triggers";
	}

	/**
	 * Class for form response
	 * 
	 * @author Dawid
	 * @version 1
	 * @since 2018-11-27
	 *
	 */
	public class TriggerResponse {
		private Long triggerId;
		private String value;
		private String type;
		private Integer resourceId;
		private Integer actionResourceId;
		private String actionValue;

		/**
		 * @return the triggerId
		 */
		public Long getTriggerId() {
			return triggerId;
		}

		/**
		 * @param triggerId the triggerId to set
		 */
		public void setTriggerId(Long triggerId) {
			this.triggerId = triggerId;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * @param value the value to set
		 */
		public void setValue(String value) {
			this.value = value;
		}

		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}

		/**
		 * @return the resourceId
		 */
		public Integer getResourceId() {
			return resourceId;
		}

		/**
		 * @param resourceId the resourceId to set
		 */
		public void setResourceId(Integer resourceId) {
			this.resourceId = resourceId;
		}

		/**
		 * @return the actionResourceId
		 */
		public Integer getActionResourceId() {
			return actionResourceId;
		}

		/**
		 * @param actionResourceId the actionResourceId to set
		 */
		public void setActionResourceId(Integer actionResourceId) {
			this.actionResourceId = actionResourceId;
		}

		/**
		 * @return the actionValue
		 */
		public String getActionValue() {
			return actionValue;
		}

		/**
		 * @param actionValue the actionValue to set
		 */
		public void setActionValue(String actionValue) {
			this.actionValue = actionValue;
		}

	}

}
