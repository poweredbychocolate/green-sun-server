package dawid.brelak.greensunserver.controllers.pages;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;
import dawid.brelak.greensunserver.repositories.DeviceResourceRepository;
import dawid.brelak.greensunserver.services.ARestService;
import dawid.brelak.greensunserver.services.AnalysisService;
import dawid.brelak.greensunserver.services.DeviceAutoReadService;

/**
 * 
 * @author Dawid
 * @since 2019-01-01
 * @version 1
 *
 */
@Controller
@RequestMapping("/")
public class UserPageController {
	@Autowired
	private DeviceAutoReadService autoReadService;
	@Autowired
	private DeviceResourceRepository resourceRepository;
	@Autowired
	private ARestService aRestService;
	@Autowired
	private AnalysisService analysisService;

	@GetMapping
	public String deviceResourcesList(Model model) {
		List<DeviceResource> allList = autoReadService.getResourcesList();
		List<DeviceResource> notList = allList.stream().filter(dr -> dr.getCurrentState() == null)
				.collect(Collectors.toList());
		List<BooleanResource> booleanList = allList.stream()
				.filter(dr -> dr instanceof BooleanResource && dr.getCurrentState() != null)
				.map(dr -> (BooleanResource) dr).collect(Collectors.toList());
		List<CharacterResource> characterList = allList.stream()
				.filter(dr -> dr instanceof CharacterResource && dr.getCurrentState() != null)
				.map(dr -> (CharacterResource) dr).collect(Collectors.toList());
		List<IntegerResource> integerList = allList.stream()
				.filter(dr -> dr instanceof IntegerResource && dr.getCurrentState() != null)
				.map(dr -> (IntegerResource) dr).collect(Collectors.toList());
		List<FloatResource> floatList = allList.stream()
				.filter(dr -> dr instanceof FloatResource && dr.getCurrentState() != null).map(dr -> (FloatResource) dr)
				.collect(Collectors.toList());
		model.addAttribute("notResources", notList);
		model.addAttribute("booleanResources", booleanList);
		model.addAttribute("characterResources", characterList);
		model.addAttribute("integerResources", integerList);
		model.addAttribute("floatResources", floatList);
		return "pages/devices";
	}

	@GetMapping
	@RequestMapping("/control")
	public String controlDeviceResourcesList(Model model) {
		List<DeviceResource> allList = autoReadService.getResourcesList();
		List<DeviceResource> notList = allList.stream().filter(dr -> dr.getCurrentState() == null && !dr.getReadOnly())
				.collect(Collectors.toList());
		List<BooleanResource> booleanList = allList.stream()
				.filter(dr -> dr instanceof BooleanResource && dr.getCurrentState() != null && !dr.getReadOnly())
				.map(dr -> (BooleanResource) dr).collect(Collectors.toList());
		List<CharacterResource> characterList = allList.stream()
				.filter(dr -> dr instanceof CharacterResource && dr.getCurrentState() != null && !dr.getReadOnly())
				.map(dr -> (CharacterResource) dr).collect(Collectors.toList());
		List<IntegerResource> integerList = allList.stream()
				.filter(dr -> dr instanceof IntegerResource && dr.getCurrentState() != null && !dr.getReadOnly())
				.map(dr -> (IntegerResource) dr).collect(Collectors.toList());
		List<FloatResource> floatList = allList.stream()
				.filter(dr -> dr instanceof FloatResource && dr.getCurrentState() != null && !dr.getReadOnly())
				.map(dr -> (FloatResource) dr).collect(Collectors.toList());
		model.addAttribute("notResources", notList);
		model.addAttribute("booleanResources", booleanList);
		model.addAttribute("characterResources", characterList);
		model.addAttribute("integerResources", integerList);
		model.addAttribute("floatResources", floatList);
		model.addAttribute("controlResponse", new ControlResponse());
		return "pages/control";
	}

	@PostMapping
	@RequestMapping("/control_")
	public String controlWrite(Model model, ControlResponse controlResponse) {
		System.err.println("[Control] : " + controlResponse.resourceId);
		System.err.println("[Control] : " + controlResponse.value);
		DeviceResource deviceResource = resourceRepository.findById(controlResponse.getResourceId()).orElse(null);
		DeviceResource responseResource = null;
		if (resourceRepository != null) {
			if (deviceResource instanceof BooleanResource) {
				String value = null;
				if (controlResponse.getValue().equals(((BooleanResource) deviceResource).getOnLabel())) {
					value = ((BooleanResource) deviceResource).getOffLabel();
				} else if (controlResponse.getValue().equals(((BooleanResource) deviceResource).getOffLabel())) {
					value = ((BooleanResource) deviceResource).getOnLabel();
				}
				responseResource = aRestService.writeResource((BooleanResource) deviceResource, value);
			} else if (deviceResource instanceof IntegerResource) {
				try {
					Integer value = Integer.parseInt(controlResponse.getValue());
					responseResource = aRestService.writeResource((IntegerResource) deviceResource, value);
				} catch (Exception e) {
				}
			} else if (deviceResource instanceof FloatResource) {
				try {
					Float value = Float.parseFloat(controlResponse.getValue());
					responseResource = aRestService.writeResource((FloatResource) deviceResource, value);
				} catch (Exception e) {
				}
			} else if (deviceResource instanceof CharacterResource
					&& !StringUtils.isEmpty(controlResponse.getValue())) {
				responseResource = aRestService.writeResource((CharacterResource) deviceResource,
						controlResponse.getValue());
			}
			if (responseResource != null) {
				autoReadService.replaceResource(responseResource);
				
			}
			analysisService.usedResource(LocalDate.now(), deviceResource);
		}

		return "forward:/control";
	}

	@GetMapping
	@RequestMapping("/login")
	public String loginPage(Model model) {
		return "login";
	}

	@GetMapping
	@RequestMapping("/logout")
	public String logout(Model model) {
		return "redirect:/";
	}

	class ControlResponse {
		private Integer resourceId = null;
		private String value = null;

		public ControlResponse() {
			// TODO Auto-generated constructor stub
		}

		/**
		 * @return the resourceId
		 */
		public Integer getResourceId() {
			return resourceId;
		}

		/**
		 * @param resourceId the resourceId to set
		 */
		public void setResourceId(Integer resourceId) {
			this.resourceId = resourceId;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * @param value the value to set
		 */
		public void setValue(String value) {
			this.value = value;
		}

	}

}
