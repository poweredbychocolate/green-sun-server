package dawid.brelak.greensunserver.controllers.rest;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;
import dawid.brelak.greensunserver.services.ARestService;
import dawid.brelak.greensunserver.services.AnalysisService;
import dawid.brelak.greensunserver.services.DeviceAutoReadService;

/**
 * Rest Controller for online {@link Device}
 * 
 * @author Dawid
 * @since 2018-12-03
 * @version 1
 *
 */
@RestController
@RequestMapping("/api/online")
public class OnlineDeviceRestController {

	@Autowired
	private DeviceAutoReadService readService;
	@Autowired
	private ARestService aRestService;
	@Autowired
	private AnalysisService analysisService;

	/**
	 * Online {@link Device} list
	 * @return device list 
	 */
	@GetMapping("/devices")
	private List<Device> devicesList() {
		List<Device> list = readService.getDevicesList();
		return list;
	}
	/**
	 * Single {@link Device}
	 * @param id
	 * @return 
	 */
	@GetMapping("/device/{id}")
	private Object getDevice(@PathVariable Integer id) {
		Device device = readService.getDevicesList().stream().filter(d -> d.getId().equals(id)).findFirst()
				.orElse(null);
		if (device == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return device;
	}
	/**
	 * Online {@link DeviceResource} list 
	 * @return list
	 */
	@GetMapping("/resources")
	private List<DeviceResource> resourcesList() {
		List<DeviceResource> list = readService.getResourcesListReadOnlyFirst();
		return list;
	}
	/**
	 * {@link DeviceResource} list filtered by {@link DeviceResource#getReadOnly()} 
	 * @param readOnly
	 * @return
	 */
	@GetMapping("/resources/{readOnly}")
	private List<DeviceResource> resourcesList(@PathVariable Boolean readOnly) {
		List<DeviceResource> list = readService.getResourcesListByMonthlyUsage(readOnly);
		return list;
	}
	/**
	 * Single {@link DeviceResource}
	 * @param id
	 * @return
	 */
	@GetMapping("/resource/{id}")
	private Object getResource(@PathVariable Integer id) {
		DeviceResource resource = readService.getResourcesList().stream().filter(d -> d.getId().equals(id)).findFirst()
				.orElse(null);
		if (resource == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return resource;
	}
	/**
	 * Set {@link DeviceResource#setCurrentState(Object)}
	 * @param id -selected resource id
	 * @param value- value to set
	 * @return resource response
	 */
	@GetMapping("/resource/{id}/{value}")
	private Object writeResource(@PathVariable Integer id, @PathVariable String value) {
		DeviceResource resource = readService.getResourcesList().stream().filter(d -> d.getId().equals(id)).findFirst()
				.orElse(null);
		if (resource != null) {
			if (resource instanceof BooleanResource) {
				BooleanResource booleanResource = (BooleanResource) resource;
				if (value.equals(booleanResource.getOnLabel()) || value.equals(booleanResource.getOffLabel())) {
					resource = aRestService.writeResource(booleanResource, value);
					readService.replaceResource(resource);
					analysisService.usedResource(LocalDate.now(), resource);
					return resource;
				}
			} else if (resource instanceof IntegerResource) {
				try {
					Integer v = Integer.parseInt(value);
					IntegerResource integerResource = (IntegerResource) resource;
					if (v >= integerResource.getMinValue() && v <= integerResource.getMaxValue()) {
						resource = aRestService.writeResource(integerResource, v);
						readService.replaceResource(resource);
						analysisService.usedResource(LocalDate.now(), resource);
						return resource;
					}
				} catch (Exception e) {
					// Cannot convert value to Integer
				}
			} else if (resource instanceof FloatResource) {
				try {
					Float f = Float.parseFloat(value);
					FloatResource floatResource = (FloatResource) resource;
					if (f >= floatResource.getMinValue() && f <= floatResource.getMaxValue()) {
						resource = aRestService.writeResource(floatResource, f);
						readService.replaceResource(resource);
						analysisService.usedResource(LocalDate.now(), resource);
						return resource;
					}
				} catch (Exception e) {
					// Cannot convert value to Float
				}

			}
			if (resource instanceof CharacterResource) {
				CharacterResource characterResource = (CharacterResource) resource;
				if (value.length() <= characterResource.getMaxLength()) {
					resource = aRestService.writeResource(characterResource, value);
					readService.replaceResource(resource);
					analysisService.usedResource(LocalDate.now(), resource);
					return resource;
				}
			}
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}
}
