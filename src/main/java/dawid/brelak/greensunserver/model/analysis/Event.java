package dawid.brelak.greensunserver.model.analysis;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters.LocalDateTimeConverter;

/**
 * Event basic log entity
 * 
 * @author Dawid
 * @since 2018-11-30
 * @version 1
 */
@Entity
public class Event implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Convert(converter = LocalDateTimeConverter.class)
	@Column(nullable = false)
	private LocalDateTime eventDate;
	@Column(nullable = false)
	private String eventOwner;
	@Column(nullable = true)
	private String eventObject;
	@Lob
	@Column(nullable = false)
	private String eventInfo;

	/**
	 * 
	 */
	public Event() {
	}

	/**
	 * @param id
	 * @param eventDate
	 * @param eventOwner
	 * @param eventInfo
	 */
	public Event(LocalDateTime eventDate, String eventOwner, String eventObject, String eventInfo) {
		super();
		this.eventObject = eventObject;
		this.eventDate = eventDate;
		this.eventOwner = eventOwner;
		this.eventInfo = eventInfo;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the eventDate
	 */
	public LocalDateTime getEventDate() {
		return eventDate;
	}

	/**
	 * @param eventDate the eventDate to set
	 */
	public void setEventDate(LocalDateTime eventDate) {
		this.eventDate = eventDate;
	}

	/**
	 * @return the eventOwner
	 */
	public String getEventOwner() {
		return eventOwner;
	}
	
	/**
	 * @return the eventObject
	 */
	public String getEventObject() {
		return eventObject;
	}

	/**
	 * @param eventObject the eventObject to set
	 */
	public void setEventObject(String eventObject) {
		this.eventObject = eventObject;
	}

	/**
	 * @param eventOwner the eventOwner to set
	 */
	public void setEventOwner(String eventOwner) {
		this.eventOwner = eventOwner;
	}

	/**
	 * @return the eventInfo
	 */
	public String getEventInfo() {
		return eventInfo;
	}

	/**
	 * @param eventInfo the eventInfo to set
	 */
	public void setEventInfo(String eventInfo) {
		this.eventInfo = eventInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Event [id=" + id + ", eventDate=" + eventDate + ", eventOwner=" + eventOwner + ", eventInfo="
				+ eventInfo + "]";
	}

}