package dawid.brelak.greensunserver.model.analysis;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import dawid.brelak.greensunserver.model.device.basic.DeviceResource;

/**
 * Monthly Resource Usage Entity 
 * @author Dawid
 * @since 2018-12-16
 * @version 1
 */
@Entity
@IdClass(MonthlyResourceUsageId.class)
public class MonthlyResourceUsage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Integer year;
	@Id
	private Integer month;
	@Id
	private Integer resourceId;
	@Column(nullable = false)
	@Min(value = 0)
	private Long usageCount = 0L;
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "device_resource_Id")
	private DeviceResource deviceResource;

	/**
	 * 
	 */
	public MonthlyResourceUsage() {

	}

	/**
	 * 
	 * @param year
	 * @param month
	 * @param resourceId
	 * @param usageCount
	 * @param deviceResource
	 */
	public MonthlyResourceUsage(Integer year, Integer month, Integer resourceId, @NotBlank @Min(0) Long usageCount,
			@NotNull DeviceResource deviceResource) {
		super();
		this.year = year;
		this.month = month;
		this.resourceId = resourceId;
		this.usageCount = usageCount;
		this.deviceResource = deviceResource;
	}

	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}

	/**
	 * @return the resourceId
	 */
	public Integer getResourceId() {
		return resourceId;
	}

	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(Integer resourceId) {
		this.resourceId = resourceId;
	}

	/**
	 * @return the usageCount
	 */
	public Long getUsageCount() {
		return usageCount;
	}

	/**
	 * @param usageCount the usageCount to set
	 */
	public void setUsageCount(Long usageCount) {
		this.usageCount = usageCount;
	}

	/**
	 * @return the deviceResource
	 */
	public DeviceResource getDeviceResource() {
		return deviceResource;
	}

	/**
	 * @param deviceResource the deviceResource to set
	 */
	public void setDeviceResource(DeviceResource deviceResource) {
		this.deviceResource = deviceResource;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(month, resourceId, usageCount, year);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonthlyResourceUsage other = (MonthlyResourceUsage) obj;
		return Objects.equals(month, other.month) && Objects.equals(resourceId, other.resourceId)
				&& Objects.equals(usageCount, other.usageCount) && Objects.equals(year, other.year);
	}

}
