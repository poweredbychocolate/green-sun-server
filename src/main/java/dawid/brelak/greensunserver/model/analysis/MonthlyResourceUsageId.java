package dawid.brelak.greensunserver.model.analysis;

import java.io.Serializable;
import java.util.Objects;
/**
 * Multiple column key for {@link MonthlyResourceUsage}
 * @author Dawid
 * @since 2018-12-16
 * @version 1
 */
public class MonthlyResourceUsageId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer year;
	private Integer month;
	private Integer resourceId;
	/**
	 * 
	 */
	public MonthlyResourceUsageId(){
		
	}
	
	/**
	 * 
	 * @param year
	 * @param month
	 * @param resourceId
	 */
	public MonthlyResourceUsageId(Integer year, Integer month, Integer resourceId) {
		super();
		this.year = year;
		this.month = month;
		this.resourceId = resourceId;
	}


	/**
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(Integer year) {
		this.year = year;
	}

	/**
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(Integer month) {
		this.month = month;
	}

	/**
	 * @return the resourceId
	 */
	public Integer getResourceId() {
		return resourceId;
	}

	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(Integer resourceId) {
		this.resourceId = resourceId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(month, resourceId, year);
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonthlyResourceUsageId other = (MonthlyResourceUsageId) obj;
		return Objects.equals(month, other.month) && Objects.equals(resourceId, other.resourceId)
				&& Objects.equals(year, other.year);
	}

}
