package dawid.brelak.greensunserver.model.arest;

import java.io.Serializable;

/**
 * 
 * @author Dawid
 * @since 2018-11-16
 * @version 1
 */
public class BooleanResponse extends DeviceResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private String valueName;
	private Boolean value;

	/**
	 * @return the valueName
	 */
	public String getValueName() {
		return valueName;
	}

	/**
	 * @param valueName the valueName to set
	 */
	public void setValueName(String valueName) {
		this.valueName = valueName;
	}

	/**
	 * @return the value
	 */
	public Boolean getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Boolean value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BooleanResponse [valueName=" + valueName + ", value=" + value + ", " + super.toString() + "]";
	}

}
