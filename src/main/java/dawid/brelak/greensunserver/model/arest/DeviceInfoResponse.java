package dawid.brelak.greensunserver.model.arest;

import java.io.Serializable;
import java.util.Map;

/**
 * Default device response
 * @author Dawid
 * @since 2018-11-15
 * @version 1
 */
public class DeviceInfoResponse extends DeviceResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String, Object> variables;


	/**
	 * @return the variables
	 */
	public Map<String, Object> getVariables() {
		return variables;
	}

	/**
	 * @param variables the variables to set
	 */
	public void setVariables(Map<String, Object> variables) {
		this.variables = variables;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DeviceInfoResponse [variables=" + variables + "," + super.toString() + "]";
	}

}