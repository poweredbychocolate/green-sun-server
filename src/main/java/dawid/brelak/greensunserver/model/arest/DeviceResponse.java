package dawid.brelak.greensunserver.model.arest;

/**
 * JSON response devices powered by ARest Library
 * 
 * @since 2018-11-15
 * @author Dawid
 * @version 1
 */
public abstract class DeviceResponse {
	private Integer id;
	private String name;
	private String hardware;
	private Boolean connected;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the hardware
	 */
	public String getHardware() {
		return hardware;
	}

	/**
	 * @param hardware the hardware to set
	 */
	public void setHardware(String hardware) {
		this.hardware = hardware;
	}

	/**
	 * @return the connected
	 */
	public Boolean getConnected() {
		return connected;
	}

	/**
	 * @param connected the connected to set
	 */
	public void setConnected(Boolean connected) {
		this.connected = connected;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DeviceResponse [id=" + id + ", name=" + name + ", hardware=" + hardware + ", connected=" + connected
				+ "]";
	}
}
