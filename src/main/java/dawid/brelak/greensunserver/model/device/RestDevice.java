package dawid.brelak.greensunserver.model.device;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

import dawid.brelak.greensunserver.model.device.basic.Device;
/**
 * {@link Device} controlled by Rest Api
 * @author Dawid
 * @since 2018-11-01
 * @version 2.0
 */
@Entity
public class RestDevice extends Device {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(nullable = false, unique = true)
	@NotBlank
	private String address;

	public RestDevice() {
		super();
	}

	/**
	 * @param address
	 */
	public RestDevice(String name, String address, String description) {
		super(name, description);
		this.address = address;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof RestDevice)) {
			return false;
		}
		RestDevice other = (RestDevice) obj;
		return Objects.equals(address, other.address)&&super.equals(obj);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RestDevice [address=" + address + ", id=" + getId() + ", name=" + getName()
				+ ", description=" + getDescription() + "]";
	}
	

}
