package dawid.brelak.greensunserver.model.device.basic;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;

/**
 * DeviceResource - Basic entity
 * @author Dawid
 * @since 2018-11-01
 * @version 2.0
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME,include=JsonTypeInfo.As.PROPERTY,property="type")
@JsonSubTypes({
	@Type(value=BooleanResource.class, name="BooleanResource"),
	@Type(value=IntegerResource.class, name="IntegerResource"),
	@Type(value=FloatResource.class, name="FloatResource"),
	@Type(value=CharacterResource.class, name="CharacterResource")
})
public abstract class DeviceResource implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(nullable = false)
	@NotBlank
	private String name;
	@Column(nullable = false)
	@NotBlank
	private String resource;
	@Column(nullable = false)
	@NonNull
	private Boolean readOnly = false;
	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "device_Id", nullable = false)
	private Device device;

	/**
	 * 
	 * @return
	 */
	public abstract Object getCurrentState();

	/**
	 * Set current state or throw {@link IncorrectResourceStateException} if value
	 * is incorrect or value is out of range
	 * 
	 * @param state
	 * @throws IncorrectResourceStateException
	 */
	public abstract boolean setCurrentState(Object state); 

	public DeviceResource() {
	}


	/**
	 * 
	 * @param name
	 * @param resource
	 * @param device
	 */
	public DeviceResource(String name, String resource, Device device) {
		this.resource = resource;
		this.name = name;
		this.device = device;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}

	/**
	 * @param resource the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
		if (this.name == null)
			name = resource;
	}

	/**
	 * @return the readOnly
	 */
	public Boolean getReadOnly() {
		return readOnly;
	}

	/**
	 * @param readOnly the readOnly to set
	 */
	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * @return the device
	 */
	public Device getDevice() {
		return device;
	}

	/**
	 * @param device the device to set
	 */
	public void setDevice(Device device) {
		this.device = device;
	}
}
