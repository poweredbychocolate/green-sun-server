package dawid.brelak.greensunserver.model.device.resources;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
/**
 * Boolean {@link DeviceResource} can be set only user defined value resource on and resource off
 * @author Dawid
 * @since 2018-11-01
 * @version 3.0
 */
@Entity
public class BooleanResource extends DeviceResource {
	
	public static final int TRUE=1;
	public static final int FALSE=0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Transient
	private String currentState;
	@Column(nullable = false)
	@NotNull
	private String onLabel;
	@Column(nullable = false)
	@NotNull
	private String offLabel;

	public BooleanResource() {
	}

	/**
	 * @param currentState
	 * @param onLabel
	 * @param offLabel
	 */
	public BooleanResource(String name,String resource, Device device, String currentState, String onLabel, String offLabel) {
		super(name,resource, device);
		this.currentState = currentState;
		this.onLabel = onLabel;
		this.offLabel = offLabel;
	}

	@Override
	public Object getCurrentState() {
		return currentState;
	}

	@Override
	public boolean setCurrentState(Object state) {
		if (state instanceof String && (((String) state).equals(onLabel) || ((String) state).equals(offLabel))) {
			this.currentState = (String) state;
			return true;
		} else {
			this.currentState = null;
			return false;
		}

	}

	/**
	 * @return the onLabel
	 */
	public String getOnLabel() {
		return onLabel;
	}

	/**
	 * @param onValue the onLabel to set
	 */
	public void setOnLabel(String onLabel) {
		this.onLabel = onLabel;
	}

	/**
	 * @return the offLabel
	 */
	public String getOffLabel() {
		return offLabel;
	}

	/**
	 * @param offValue the offLabel to set
	 */
	public void setOffLabel(String offLabel) {
		this.offLabel = offLabel;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BooleanResource [currentState=" + currentState + ", onLabel=" + onLabel + ", offLabel=" + offLabel
				+ ", id=" + getId() + ", name=" + getName() + ", resource=" + getResource()
				+ ", readOnly=" + getReadOnly() + "]";
	}
	

}
