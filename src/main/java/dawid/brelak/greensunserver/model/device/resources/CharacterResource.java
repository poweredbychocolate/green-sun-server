package dawid.brelak.greensunserver.model.device.resources;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;

/**
 * Character {@link DeviceResource} limited by text length
 * 
 * @author Dawid
 * @since 2018-11-01
 * @version 2.0
 */
@Entity
public class CharacterResource extends DeviceResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Transient
	private String currentState;
	@Column(nullable = false)
	@NotNull
	private Integer maxLength = 0;

	public CharacterResource() {
	}

	/**
	 * @param currentState
	 * @param maxLength
	 */
	public CharacterResource(String name,String resource, Device device, String currentState, Integer maxLength) {
		super(name,resource, device);
		this.currentState = currentState;
		this.maxLength = maxLength;
	}

	@Override
	public Object getCurrentState() {
		return currentState;
	}

	@Override
	public boolean setCurrentState(Object state) {
		if (state instanceof String && ((String) state).length() <= maxLength) {
			this.currentState = (String) state;
			return true;
		} else {
			this.currentState = null;
			return false;
		}

	}

	/**
	 * @return the maxLength
	 */
	public Integer getMaxLength() {
		return maxLength;
	}

	/**
	 * @param maxLength the maxLength to set
	 */
	public void setMaxLength(Integer maxLength) {
		this.maxLength = maxLength;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CharacterResource [currentState=" + currentState + ", maxLength=" + maxLength + ", id=" + getId()
				+ ", name=" + getName() + ", resource=" + getResource() + ", readOnly=" + getReadOnly()
				+ "]";
	}
	

}
