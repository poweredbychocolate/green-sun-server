package dawid.brelak.greensunserver.model.device.resources;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;

/**
 * Integer {@link DeviceResource} that can be set integer value for user define
 * range from {@link #getMinValue()} to {@link #getMaxValue()}
 * 
 * @author Dawid
 * @since 2018-11-01
 * @version 2.0
 */
@Entity
public class IntegerResource extends DeviceResource {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Transient
	private Integer currentState;
	@Column(nullable = false)
	@NotNull
	private Integer minValue;
	@Column(nullable = false)
	@NotNull
	private Integer maxValue;

	public IntegerResource() {
	}

	/**
	 * @param currentState
	 * @param minValue
	 * @param maxValue
	 */
	public IntegerResource(String name, String resource, Device device, Integer currentState, Integer minValue,
			Integer maxValue) {
		super(name, resource, device);
		this.currentState = currentState;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	@Override
	public Object getCurrentState() {
		return currentState;
	}

	@Override
	public boolean setCurrentState(Object state) {
		if (state instanceof Integer && (Integer) state >= minValue && (Integer) state <= maxValue) {
			this.currentState = (Integer) state;
			return true;
		} else {
			this.currentState = null;
			return false;
		}

	}

	/**
	 * @return the minValue
	 */
	public Integer getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue the minValue to set
	 */
	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the maxValue
	 */
	public Integer getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue the maxValue to set
	 */
	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "IntegerResource [currentState=" + currentState + ", minValue=" + minValue + ", maxValue=" + maxValue
				+ ", id=" + getId() + ", name=" + getName() + ", resource=" + getResource() + ", readOnly="
				+ getReadOnly() + "]";
	}

}
