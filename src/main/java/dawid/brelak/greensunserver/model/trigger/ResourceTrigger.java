package dawid.brelak.greensunserver.model.trigger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import dawid.brelak.greensunserver.model.device.basic.DeviceResource;

/**
 * Basic ResourceTrigger entity
 * @author Dawid
 * @version 1
 * @since 2018-12-27
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class ResourceTrigger implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "device_resource_Id")
	private DeviceResource sourceResource;
	@NotBlank
	@Column(nullable = false)
	private String sourceValue;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "trigger_Id")
	private List<TriggerAction> triggerActions;

	public ResourceTrigger() {
		triggerActions = new ArrayList<TriggerAction>();
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the sourceResource
	 */
	public DeviceResource getSourceResource() {
		return sourceResource;
	}

	/**
	 * @param sourceResource the sourceResource to set
	 */
	public void setSourceResource(DeviceResource sourceResource) {
		this.sourceResource = sourceResource;
	}

	/**
	 * @return the sourceValue
	 */
	public String getSourceValue() {
		return sourceValue;
	}

	/**
	 * @param sourceValue the sourceValue to set
	 */
	public void setSourceValue(String sourceValue) {
		this.sourceValue = sourceValue;
	}

	/**
	 * @return the triggerActions
	 */
	public List<TriggerAction> getTriggerActions() {
		return triggerActions;
	}

	/**
	 * @param triggerActions the triggerActions to set
	 */
	public void setTriggerActions(List<TriggerAction> triggerActions) {
		this.triggerActions = triggerActions;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResourceTrigger [id=" + id + ", sourceResource=" + sourceResource + ", sourceValue=" + sourceValue
				+ ", triggerActions=" + triggerActions + "]";
	}

}
