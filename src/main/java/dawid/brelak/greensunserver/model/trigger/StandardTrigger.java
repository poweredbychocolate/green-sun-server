package dawid.brelak.greensunserver.model.trigger;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

/**
 * Standard {@link TriggerAction} with type selector 
 * @author Dawid
 * @version 1
 * @since 2018-12-27
 *
 */
@Entity
public class StandardTrigger extends ResourceTrigger {

	// public static final String TYPE_="";
	public static final String TYPE_EQUALS_SINGLE = "EQUALS_SINGLE";
	public static final String TYPE_BELOW_SINGLE = "BELOW_SINGLE";
	public static final String TYPE_ABOVE_SINGLE = "ABOVE_SINGLE";
	public static final String TYPE_EQUALS_REPEAT = "EQUALS_REPEAT";
	public static final String TYPE_BELOW_REPEAT = "BELOW_REPEAT";
	public static final String TYPE_ABOVE_REPEAT = "ABOVE_REPEAT";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotBlank
	@Column(nullable = false)
	private String triggerType;

	/**
	 * @return the triggerType
	 */
	public String getTriggerType() {
		return triggerType;
	}

	/**
	 * @param triggerType the triggerType to set
	 */
	public void setTriggerType(String triggerType) {
		this.triggerType = triggerType;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StandardTrigger [triggerType=" + triggerType + ", getTriggerType()=" + getTriggerType()
				+ ", toString()=" + super.toString() + "]";
	}

	/**
	 * Return all types list
	 * @return types list 
	 */
	public static List<String> getTypes() {
		ArrayList<String> list = new ArrayList<>();
		list.add(TYPE_EQUALS_SINGLE);
		list.add(TYPE_BELOW_SINGLE);
		list.add(TYPE_ABOVE_SINGLE);
		list.add(TYPE_EQUALS_REPEAT);
		list.add(TYPE_BELOW_REPEAT);
		list.add(TYPE_ABOVE_REPEAT);
		return list;
	}

}
