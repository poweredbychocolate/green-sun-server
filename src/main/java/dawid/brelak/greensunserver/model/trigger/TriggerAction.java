package dawid.brelak.greensunserver.model.trigger;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import dawid.brelak.greensunserver.model.device.basic.DeviceResource;

/**
 * Action Entity contains {@link DeviceResource} and value for it
 * @author Dawid
 * @version 1
 * @since 2018-12-27
 */
@Entity
public class TriggerAction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="device_resource_Id")
	private DeviceResource resource;
	@NotBlank
	@Column(nullable=false)
	private String value;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the resource
	 */
	public DeviceResource getResource() {
		return resource;
	}
	/**
	 * @param resource the resource to set
	 */
	public void setResource(DeviceResource resource) {
		this.resource = resource;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TriggerAction [id=" + id + ", resource=" + resource + ", value=" + value + "]";
	}

}
