package dawid.brelak.greensunserver.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.repositories.extensions.DeviceResourceExtension;

/**
 * Repository for {@link DeviceResource}
 * 
 * @author Dawid
 * @since 2018-11-13
 * @version 1.0
 */
@Repository
public interface DeviceResourceRepository extends JpaRepository<DeviceResource, Integer>, DeviceResourceExtension {

	

}
