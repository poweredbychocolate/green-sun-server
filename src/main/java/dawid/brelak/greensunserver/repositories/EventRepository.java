package dawid.brelak.greensunserver.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dawid.brelak.greensunserver.model.analysis.Event;
import dawid.brelak.greensunserver.repositories.extensions.EventExtension;

/**
 * Repository for {@link Event}
 * 
 * @author Dawid
 * @since 2018-11-30
 * @version 1
 */
@Repository
public interface EventRepository extends JpaRepository<Event, Long>,EventExtension {

}
