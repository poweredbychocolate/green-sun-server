package dawid.brelak.greensunserver.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsage;
import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsageId;
import dawid.brelak.greensunserver.repositories.extensions.MontlyUsageRepositoryExtension;

@Repository
public interface MontlyUsageRepository extends JpaRepository<MonthlyResourceUsage, MonthlyResourceUsageId>,MontlyUsageRepositoryExtension {

}
