package dawid.brelak.greensunserver.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.repositories.extensions.RestDeviceExtension;

/**
 * Repository for {@link RestDevice}
 * 
 * @author Dawid
 * @since 2018-11-01
 * @version 1.0
 */
@Repository
public interface RestDeviceRepository extends JpaRepository<RestDevice, Integer>, RestDeviceExtension {

}
