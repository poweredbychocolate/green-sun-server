package dawid.brelak.greensunserver.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dawid.brelak.greensunserver.model.trigger.ResourceTrigger;
import dawid.brelak.greensunserver.repositories.extensions.TriggerExtension;



@Repository
public interface TriggerRepository extends JpaRepository<ResourceTrigger, Long>,TriggerExtension {

}
