package dawid.brelak.greensunserver.repositories.extensions;

import java.util.List;

import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.repositories.DeviceResourceRepository;

/**
 * Additional function for {@link DeviceResourceRepository}
 * 
 * @author Dawid
 * @since 2018-11-13
 * @version 1.0
 */
public interface DeviceResourceExtension {
	/**
	 * Get new {@link DeviceResource} by class name
	 * @param name
	 * @return
	 */
	public DeviceResource newByName(String name);
	/**
	 * Read {@link DeviceResource} list filtered by {@link DeviceResource#getReadOnly()}
	 * @param readOnly
	 * @return
	 */
	List<DeviceResource> findAllByReadOnly(boolean readOnly);

}
