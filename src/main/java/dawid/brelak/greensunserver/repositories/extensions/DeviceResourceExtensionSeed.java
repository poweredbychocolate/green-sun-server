package dawid.brelak.greensunserver.repositories.extensions;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource_;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;

/**
 * {@link DeviceResourceExtension} method implementation
 * 
 * @author Dawid
 * @since 2018-11-13
 * @version 1.0
 */
public class DeviceResourceExtensionSeed implements DeviceResourceExtension {
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public DeviceResource newByName(String name) {
		if (name.equals("CharacterResource")) {
			return new CharacterResource();
		} else if (name.equals("BooleanResource")) {
			return new BooleanResource();
		} else if (name.equals("IntegerResource")) {
			return new IntegerResource();
		} else if (name.equals("FloatResource")) {
			return new FloatResource();
		}
	return null;
	}

	@Override
	@Transactional
	public List<DeviceResource> findAllByReadOnly(boolean readOnly) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<DeviceResource> query = builder.createQuery(DeviceResource.class);
		Root<DeviceResource> root = query.from(DeviceResource.class);
		query.where(builder.equal(root.get(DeviceResource_.readOnly), readOnly));
		TypedQuery<DeviceResource> typedQuery = entityManager.createQuery(query);
		List<DeviceResource> list = typedQuery.getResultList();
		return list;
	}

}
