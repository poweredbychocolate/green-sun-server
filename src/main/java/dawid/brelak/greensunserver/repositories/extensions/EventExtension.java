package dawid.brelak.greensunserver.repositories.extensions;

import dawid.brelak.greensunserver.model.analysis.Event;
import dawid.brelak.greensunserver.repositories.EventRepository;

/**
 * Additional function for {@link EventRepository}
 * 
 * @author Dawid
 * @since 2018-12-03
 * @version 1
 */
public interface EventExtension {
	/**
	 * Save object if not exist or was written above 30 minutes ago
	 * @param event
	 * @return
	 */
	public boolean checkAndSave(Event event);
}
