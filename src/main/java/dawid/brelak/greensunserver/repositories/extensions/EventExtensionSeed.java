package dawid.brelak.greensunserver.repositories.extensions;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import dawid.brelak.greensunserver.model.analysis.Event;

/**
 * {@link EventExtension} method implementation
 * 
 * @author Dawid
 * @since 2018-11-13
 * @version 1.0
 */
public class EventExtensionSeed implements EventExtension {

	@PersistenceContext
	EntityManager entityManager;
	
	private ConcurrentHashMap<String, LocalDateTime> lastEventTimeMap;
	public EventExtensionSeed() {
	lastEventTimeMap = new ConcurrentHashMap<>();
	}

	@Override
	@Transactional
	public boolean checkAndSave(Event event) {
		String key = makeKeyForTimeMap(event);
		if(lastEventTimeMap.containsKey(key)){
			LocalDateTime timeFromMap = lastEventTimeMap.get(key);
			Duration duration =Duration.between(timeFromMap,event.getEventDate());
			if(duration.toMinutes()>=30) {
				entityManager.persist(event);
				lastEventTimeMap.replace(key, event.getEventDate());
				return true;
			}else {
				return false;
			}
		}else {
			entityManager.persist(event);
			lastEventTimeMap.put(key, event.getEventDate());
			return true;
		}
	}
	
	private String makeKeyForTimeMap(Event event) {
		return event.getEventOwner()+" ::: "+event.getEventObject();
	}

}
