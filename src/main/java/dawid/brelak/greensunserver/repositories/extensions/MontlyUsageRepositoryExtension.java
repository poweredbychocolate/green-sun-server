package dawid.brelak.greensunserver.repositories.extensions;

import java.util.List;
import java.util.Set;

import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsage;
import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsageId;
import dawid.brelak.greensunserver.repositories.EventRepository;
import dawid.brelak.greensunserver.repositories.key.DataChangeKey;

/**
 * Additional function for {@link EventRepository}
 * 
 * @author Dawid
 * @since 2018-12-03
 * @version 1
 */
public interface MontlyUsageRepositoryExtension extends DataChangeKey{
	/**
	 * Find {@link MonthlyResourceUsage} using keySet
	 * @param keySet
	 * @return
	 */
	List<MonthlyResourceUsage> findAllByKeys(Set<MonthlyResourceUsageId> keySet);
	/**
	 * Find {@link MonthlyResourceUsage} by year and month
	 * @param year
	 * @param month
	 * @return
	 */
	List<MonthlyResourceUsage> findAllByYearAndMonth(Integer year,Integer month);
	/**
	 * Get entries keys selected by year and month 
	 * @param year
	 * @param month
	 * @return
	 */
	List<Integer> getMonthlyKeys(Integer year,Integer month);
	/**
	 * REmove entries by keys list
	 * @param list
	 * @return
	 */
	boolean removeByResourcesIds(List<Integer> list);
}
