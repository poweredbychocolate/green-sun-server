package dawid.brelak.greensunserver.repositories.extensions;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsage;
import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsageId;
import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsage_;
import dawid.brelak.greensunserver.repositories.key.DataChangeKeyImpl;

/**
 * {@link EventExtension} method implementation
 * 
 * @author Dawid
 * @since 2018-11-13
 * @version 1.0
 */
public class MontlyUsageRepositoryExtensionSeed extends DataChangeKeyImpl implements MontlyUsageRepositoryExtension {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	@Transactional
	public List<MonthlyResourceUsage> findAllByKeys(Set<MonthlyResourceUsageId> keySet) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<MonthlyResourceUsage> query = builder.createQuery(MonthlyResourceUsage.class);
		Root<MonthlyResourceUsage> root = query.from(MonthlyResourceUsage.class);

		List<Integer> yearKeys = keySet.stream().map(MonthlyResourceUsageId::getYear).collect(Collectors.toList());
		List<Integer> monthKeys = keySet.stream().map(MonthlyResourceUsageId::getMonth).collect(Collectors.toList());
		List<Integer> idKeys = keySet.stream().map(MonthlyResourceUsageId::getResourceId).collect(Collectors.toList());
		query.where(builder.and(root.get(MonthlyResourceUsage_.YEAR).in(yearKeys),
				root.get(MonthlyResourceUsage_.MONTH).in(monthKeys),
				root.get(MonthlyResourceUsage_.RESOURCE_ID).in(idKeys)));
		TypedQuery<MonthlyResourceUsage> typedQuery = entityManager.createQuery(query);
		List<MonthlyResourceUsage> list = typedQuery.getResultList();
		return list;
	}

	@Override
	public List<MonthlyResourceUsage> findAllByYearAndMonth(Integer year, Integer month) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<MonthlyResourceUsage> query = builder.createQuery(MonthlyResourceUsage.class);
		Root<MonthlyResourceUsage> root = query.from(MonthlyResourceUsage.class);
		root.fetch(MonthlyResourceUsage_.DEVICE_RESOURCE);
		query.where(builder.and(builder.equal(root.get(MonthlyResourceUsage_.YEAR), year),
				builder.equal(root.get(MonthlyResourceUsage_.MONTH), month)));
		query.orderBy(builder.desc(root.get(MonthlyResourceUsage_.USAGE_COUNT)));
		TypedQuery<MonthlyResourceUsage> typedQuery = entityManager.createQuery(query);
		List<MonthlyResourceUsage> list = typedQuery.getResultList();
		return list;
	}

	@Override
	public List<Integer> getMonthlyKeys(Integer year, Integer month) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Integer> query = builder.createQuery(Integer.class);
		Root<MonthlyResourceUsage> root = query.from(MonthlyResourceUsage.class);
		query.select(root.get(MonthlyResourceUsage_.RESOURCE_ID));
		query.where(builder.and(builder.equal(root.get(MonthlyResourceUsage_.YEAR), year),
				builder.equal(root.get(MonthlyResourceUsage_.MONTH), month)));
		query.orderBy(builder.desc(root.get(MonthlyResourceUsage_.USAGE_COUNT)));
		TypedQuery<Integer> typedQuery = entityManager.createQuery(query);
		List<Integer> list = typedQuery.getResultList();
		return list;

	}

	@Override
	@Transactional
	public boolean removeByResourcesIds(List<Integer> list) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaDelete<MonthlyResourceUsage> query = builder.createCriteriaDelete(MonthlyResourceUsage.class);
		Root<MonthlyResourceUsage> root = query.from(MonthlyResourceUsage.class);
		query.where(root.get(MonthlyResourceUsage_.resourceId).in(list));
		Query deleteQuery = entityManager.createQuery(query);
		int deletes = deleteQuery.executeUpdate();
		if (deletes > 0)
			return true;
		return false;
	}

}
