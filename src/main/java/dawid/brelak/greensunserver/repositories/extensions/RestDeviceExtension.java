package dawid.brelak.greensunserver.repositories.extensions;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.repositories.RestDeviceRepository;
import dawid.brelak.greensunserver.repositories.key.DataChangeKey;

/**
 * Additional function for {@link RestDeviceRepository}
 * 
 * @author Dawid
 * @since 2018-11-01
 * @version 1.0
 */
public interface RestDeviceExtension extends DataChangeKey{
	/**
	 * Find {@link DeviceResource} 
	 * @param name
	 * @return
	 */
	public RestDevice findByName(String name);


}
