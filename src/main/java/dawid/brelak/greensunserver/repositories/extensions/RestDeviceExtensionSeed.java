package dawid.brelak.greensunserver.repositories.extensions;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.RestDevice_;
import dawid.brelak.greensunserver.repositories.key.DataChangeKeyImpl;

/**
 * {@link RestDeviceExtension} method implementation 
 * @author Dawid
 * @since 2018-11-01
 * @version 1.0
 */
public class RestDeviceExtensionSeed extends DataChangeKeyImpl implements RestDeviceExtension {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public RestDevice findByName(String name) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<RestDevice> query = builder.createQuery(RestDevice.class);
		Root<RestDevice> root = query.from(RestDevice.class);
		query.select(root);
		query.where(builder.equal(root.get(RestDevice_.name), name));
		TypedQuery<RestDevice> typedQuery = entityManager.createQuery(query);
		try {
			return typedQuery.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}


}
