package dawid.brelak.greensunserver.repositories.extensions;

import java.util.List;
import dawid.brelak.greensunserver.model.trigger.TriggerAction;
import dawid.brelak.greensunserver.repositories.TriggerRepository;
import dawid.brelak.greensunserver.repositories.key.DataChangeKey;


/**
 * Additional function for {@link TriggerRepository}
 * @author Dawid
 * @since 2018-12-28
 * @version 1
 */
public interface TriggerExtension extends DataChangeKey{
	/**
	 * Remove from database {@link TriggerAction} using keys list
	 * @param keys
	 */
	void removeActions(List<Long> keys);
	/**
	 * Remove single {@link TriggerAction}
	 * @param triggerAction
	 */
	void removeAction(TriggerAction triggerAction);
	/**
	 * Return list all {@link TriggerAction}
	 * @return
	 */
	List<TriggerAction> findAllActions();


}
