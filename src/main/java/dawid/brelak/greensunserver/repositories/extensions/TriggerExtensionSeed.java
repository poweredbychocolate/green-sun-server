package dawid.brelak.greensunserver.repositories.extensions;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import dawid.brelak.greensunserver.model.trigger.TriggerAction;
import dawid.brelak.greensunserver.model.trigger.TriggerAction_;
import dawid.brelak.greensunserver.repositories.key.DataChangeKeyImpl;

/**
 * {@link TriggerExtension} method implementation 
 * @author Dawid
 * @since 2018-12-28
 * @version 1
 */
public class TriggerExtensionSeed extends DataChangeKeyImpl implements TriggerExtension {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	@Transactional
	public void removeAction(TriggerAction triggerAction) {
		entityManager.remove(entityManager.merge(triggerAction));

	}

	@Override
	@Transactional
	public void removeActions(List<Long> keys) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaDelete<TriggerAction> query = builder.createCriteriaDelete(TriggerAction.class);
		Root<TriggerAction> root = query.from(TriggerAction.class);
		query.where(root.get(TriggerAction_.ID).in(keys));
		Query deleteQuery = entityManager.createQuery(query);
		deleteQuery.executeUpdate();
		
	}
	@Override
	@Transactional
	public List<TriggerAction> findAllActions() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<TriggerAction> query = builder.createQuery(TriggerAction.class);
		Root<TriggerAction> root = query.from(TriggerAction.class);
		TypedQuery<TriggerAction> typedQuery = entityManager.createQuery(query);
		return typedQuery.getResultList();
		
	}

}
