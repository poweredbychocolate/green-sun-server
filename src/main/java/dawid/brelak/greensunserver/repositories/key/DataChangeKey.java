package dawid.brelak.greensunserver.repositories.key;
/**
 * 
 * @author Dawid
 * @since 2018-12-17
 * @version 1
 */
public interface DataChangeKey {
	/**
	 * Notify data change
	 */
	void notifyDataChange();
	/**
	 * Get current data key
	 * @return long- data key
	 */
	Long getDataKey();

}
