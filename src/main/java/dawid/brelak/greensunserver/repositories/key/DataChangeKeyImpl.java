package dawid.brelak.greensunserver.repositories.key;

import java.util.concurrent.atomic.AtomicLong;
/**
 * Implementation of {@link DataChangeKey}
 * @author Dawid
 * @since 2018-12-17
 * @version 1
 */
public abstract class DataChangeKeyImpl implements DataChangeKey {
	private AtomicLong key;

	public DataChangeKeyImpl() {
		key = new AtomicLong(Long.MIN_VALUE);
	}

	@Override
	public void notifyDataChange() {
		key.incrementAndGet();

	}

	@Override
	public Long getDataKey() {
		return key.get();
	}

}
