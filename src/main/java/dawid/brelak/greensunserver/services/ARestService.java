package dawid.brelak.greensunserver.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dawid.brelak.greensunserver.connectors.ARestConnector;
import dawid.brelak.greensunserver.model.analysis.Event;
import dawid.brelak.greensunserver.model.arest.BooleanResponse;
import dawid.brelak.greensunserver.model.arest.CharacterResponse;
import dawid.brelak.greensunserver.model.arest.DeviceInfoResponse;
import dawid.brelak.greensunserver.model.arest.DeviceResponse;
import dawid.brelak.greensunserver.model.arest.FloatResponse;
import dawid.brelak.greensunserver.model.arest.IntegerResponse;
import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;
import dawid.brelak.greensunserver.repositories.EventRepository;

/**
 * Processing data provides by {@link ARestConnector}
 * 
 * @author Dawid
 * @since 2018-11-15
 * @version 1
 *
 */
@Service
public class ARestService {
	private static final String SUFIX = "Write";

	
	public ARestService() {
		super();
	}

	@Autowired
	private ARestConnector aRestConnector;

	@Autowired
	private EventRepository eventRepository;

	////
	private void writeLog(String object,String text) {
		Event event = new Event();
		event.setEventDate(LocalDateTime.now());
		event.setEventOwner(ARestService.class.getSimpleName());
		event.setEventObject(object);
		event.setEventInfo(text);
		eventRepository.save(event);
	}

	/**
	 * Boolean response
	 * 
	 * @param url
	 * @return
	 */
	public BooleanResponse getBooleanResponse(String url) {
		return (BooleanResponse) getByResponseType(url, BooleanResponse.class);
	}

	/**
	 * Float response
	 * 
	 * @param url
	 * @return
	 */
	public FloatResponse getFloatResponse(String url) {
		return (FloatResponse) getByResponseType(url, FloatResponse.class);
	}

	/**
	 * Integer response
	 * 
	 * @param url
	 * @return
	 */
	public IntegerResponse getIntegerResponse(String url) {
		return (IntegerResponse) getByResponseType(url, IntegerResponse.class);
	}

	/**
	 * String response
	 * 
	 * @param url
	 * @return
	 */
	public CharacterResponse getStringResponse(String url) {
		return (CharacterResponse) getByResponseType(url, CharacterResponse.class);
	}

	/**
	 * Parse response as selected {@link DeviceResponse} class
	 * 
	 * @param url
	 * @param type
	 * @return
	 */
	public DeviceResponse getByResponseType(String url, Class<?> type) {
		try {
			String stringResponse = aRestConnector.getAsString(url);
			List<String> parts = aRestConnector.splitResponse(stringResponse);
			List<String> used = new ArrayList<>();
			////

			DeviceResponse response = null;
			if (type.equals(FloatResponse.class)) {
				response = new FloatResponse();
			} else if (type.equals(IntegerResponse.class)) {
				response = new IntegerResponse();
			} else if (type.equals(CharacterResponse.class)) {
				response = new CharacterResponse();
			} else if (type.equals(BooleanResponse.class)) {
				response = new BooleanResponse();
			}
			for (int i = 0; i < parts.size(); i += 2) {
				if (parts.get(i).equals("id")) {
					response.setId(Integer.parseInt(parts.get(i + 1)));
					used.add(parts.get(i));
					used.add(parts.get(i + 1));
				} else if (parts.get(i).equals("name")) {
					response.setName(parts.get(i + 1));
					used.add(parts.get(i));
					used.add(parts.get(i + 1));
				} else if (parts.get(i).equals("hardware")) {
					response.setHardware(parts.get(1 + i));
					used.add(parts.get(i));
					used.add(parts.get(i + 1));
				} else if (parts.get(i).equals("connected")) {
					response.setConnected(Boolean.parseBoolean(parts.get(1 + i)));
					used.add(parts.get(i));
					used.add(parts.get(i + 1));
				}

			}
			for (String part : used) {
				parts.remove(part);
			}
			// parts.removeAll(used);
			if (parts.size() == 2) {
				if (type.equals(FloatResponse.class)) {
					((FloatResponse) response).setValueName(parts.get(0));
					((FloatResponse) response).setValue(Float.parseFloat(parts.get(1)));
					return response;
				} else if (type.equals(IntegerResponse.class)) {
					((IntegerResponse) response).setValueName(parts.get(0));
					((IntegerResponse) response).setValue(Integer.parseInt(parts.get(1)));
					return response;
				} else if (type.equals(CharacterResponse.class)) {
					((CharacterResponse) response).setValueName(parts.get(0));
					((CharacterResponse) response).setValue(parts.get(1));
					return response;
				} else if (type.equals(BooleanResponse.class)) {
					((BooleanResponse) response).setValueName(parts.get(0));
					((BooleanResponse) response).setValue(Boolean.parseBoolean(parts.get(1)));
					return response;
				}

			} else {
				return null;
			}

		} catch (Exception e) {

			return null;
		}
		return null;
	}

	/**
	 * Build fully http url
	 * 
	 * @param address
	 * @param resource
	 * @param value
	 * @return
	 */
	private String httpURL(String address, String resource, String value) {
		StringBuilder url = new StringBuilder();
		url.append("http://");
		url.append(address);
		if (resource != null) {
			url.append("/");
			url.append(resource);
		}
		if (value != null) {
			url.append("?param=");
			url.append(value);
		}
		return url.toString();
	}

	/**
	 * Send request with value and set
	 * {@link CharacterResource#setCurrentState(Object)}
	 * 
	 * @param resource
	 * @param value
	 * @return
	 */
	public CharacterResource writeResource(CharacterResource resource, String value) {
		resource.setCurrentState(null);
		if (value == null) {
			writeLog(resource.getResource(),"[ " + CharacterResource.class.getSimpleName() + " ] - incorrect value");
			return resource;
		}

		if (value.length() > resource.getMaxLength()) {
			writeLog(resource.getResource(),"[ " + CharacterResource.class.getSimpleName() + " ] - incorrect value");
			return resource;
		}
		String url = httpURL(((RestDevice) resource.getDevice()).getAddress(), resource.getResource() + SUFIX, value);
		IntegerResponse integerResponse = (IntegerResponse) getByResponseType(url, IntegerResponse.class);
		if (integerResponse != null && integerResponse.getValue() == 0)
			resource.setCurrentState(value);
		else {
			resource.setCurrentState(null);
			writeLog(resource.getResource(),"[ " + resource.getResource() + " ] - not responding");
		}
		return resource;
	}

	/**
	 * Send request with value and set
	 * {@link CharacterSwitchResource#setCurrentState(Object)}
	 * 
	 * @param resource
	 * @param value
	 * @return
	 */
	public BooleanResource writeResource(BooleanResource resource, String value) {
		resource.setCurrentState(null);
		if (value == null) {
			writeLog(resource.getResource(),"[ " + BooleanResource.class.getSimpleName() + " ] - incorrect value");
			return resource;
		}
		if (!value.equals(resource.getOnLabel()) && !value.equals(resource.getOffLabel())) {
			writeLog(resource.getResource(),"[ " + BooleanResource.class.getSimpleName() + " ] - incorrect value");
			return resource;
		}
		int param = BooleanResource.FALSE;
		if (value.equals(resource.getOnLabel())) {
			param = BooleanResource.TRUE;
		}
		String url = httpURL(((RestDevice) resource.getDevice()).getAddress(), resource.getResource() + SUFIX,
				Integer.toString(param));
		IntegerResponse integerResponse = (IntegerResponse) getByResponseType(url, IntegerResponse.class);
		if (integerResponse != null && integerResponse.getValue() == 0 && value.equals(resource.getOffLabel())) {
			resource.setCurrentState(resource.getOffLabel());
		} else if (integerResponse != null && integerResponse.getValue() == 0 && value.equals(resource.getOnLabel())) {
			resource.setCurrentState(resource.getOnLabel());
		} else {
			resource.setCurrentState(null);
			writeLog(resource.getResource(),"[ " + resource.getName() + " ] - not responding");
		}
		return resource;
	}

	/**
	 * Send request with value and set
	 * {@link IntegerResource#setCurrentState(Object)}
	 * 
	 * @param resource
	 * @param value
	 * @return
	 */
	public IntegerResource writeResource(IntegerResource resource, Integer value) {
		resource.setCurrentState(null);
		if (value == null) {
			writeLog(resource.getResource(),"[ " + IntegerResource.class.getSimpleName() + " ] - value out of range");
			return resource;
		}
		if (value < resource.getMinValue() || value > resource.getMaxValue()) {
			writeLog(resource.getResource(),"[ " + IntegerResource.class.getSimpleName() + " ] - value out of range");
			return resource;
		}
		String url = httpURL(((RestDevice) resource.getDevice()).getAddress(), resource.getResource() + SUFIX,
				value.toString());
		IntegerResponse integerResponse = (IntegerResponse) getByResponseType(url, IntegerResponse.class);
		if (integerResponse != null && integerResponse.getValue() == 0) {
			resource.setCurrentState(value);
		} else {
			resource.setCurrentState(null);
			writeLog(resource.getResource(),"[ " + resource.getName() + " ] - not responding");
		}
		return resource;
	}

	/**
	 * Send request with value and set
	 * {@link FloatingPointResource#setCurrentState(Object)}
	 * 
	 * @param resource
	 * @param value
	 * @return
	 */
	public FloatResource writeResource(FloatResource resource, Float value) {
		resource.setCurrentState(null);
		if (value == null) {
			writeLog(resource.getResource(),"[ " + FloatResource.class.getSimpleName() + " ] - value out of range");
			return resource;
		}
		if (value < resource.getMinValue() || value > resource.getMaxValue()) {
			writeLog(resource.getResource(),"[ " + FloatResource.class.getSimpleName() + " ] - value out of range");
			return resource;
		}
		String url = httpURL(((RestDevice) resource.getDevice()).getAddress(), resource.getResource() + SUFIX,
				value.toString());
		IntegerResponse integerResponse = (IntegerResponse) getByResponseType(url, IntegerResponse.class);
		if (integerResponse != null && integerResponse.getValue() == 0) {
			resource.setCurrentState(value);
		} else {
			writeLog(resource.getResource(),"[ " + resource.getName() + " ] - not responding");
		}

		return resource;
	}

	/**
	 * Send request that read current state of {@link IntegerResource}
	 * 
	 * @param resource
	 * @return
	 */
	public IntegerResource readResource(IntegerResource resource) {

		String url = httpURL(((RestDevice) resource.getDevice()).getAddress(), resource.getResource(), null);
		IntegerResponse integerResponse = (IntegerResponse) getByResponseType(url, IntegerResponse.class);
		if (integerResponse != null && integerResponse.getValue() >= resource.getMinValue()
				&& integerResponse.getValue() <= resource.getMaxValue()) {
			resource.setCurrentState(integerResponse.getValue());
		} else {
			resource.setCurrentState(null);
			writeLog(resource.getResource(),"[ " + resource.getName() + " ] - not responding");
		}
		return resource;
	}

	/**
	 * Send request that read current state of {@link FloatingPointResource}
	 * 
	 * @param resource
	 * @return
	 */
	public FloatResource readResource(FloatResource resource) {

		String url = httpURL(((RestDevice) resource.getDevice()).getAddress(), resource.getResource(), null);
		FloatResponse floatResponse = (FloatResponse) getByResponseType(url, FloatResponse.class);
		if (floatResponse != null && floatResponse.getValue() >= resource.getMinValue()
				&& floatResponse.getValue() <= resource.getMaxValue()) {
			resource.setCurrentState(floatResponse.getValue());
		} else {
			resource.setCurrentState(null);
			writeLog(resource.getResource(),"[ " + resource.getName() + " ] - not responding");
		}
		return resource;
	}

	/**
	 * Send request that read current state of {@link BooleanResource}
	 * 
	 * @param resource
	 * @return
	 */
	public BooleanResource readResource(BooleanResource resource) {

		String url = httpURL(((RestDevice) resource.getDevice()).getAddress(), resource.getResource(), null);
		BooleanResponse booleanResponse = (BooleanResponse) getByResponseType(url, BooleanResponse.class);
		if (booleanResponse != null && !booleanResponse.getValue()) {
			resource.setCurrentState(resource.getOffLabel());
		} else if (booleanResponse != null && booleanResponse.getValue()) {
			resource.setCurrentState(resource.getOnLabel());
		} else {
			resource.setCurrentState(null);
			writeLog(resource.getResource(),"[ " + resource.getName() + " ] - not responding");
		}
		return resource;
	}

	/**
	 * Send request that read current state of {@link CharacterResource}
	 * 
	 * @param resource
	 * @return
	 */
	public CharacterResource readResource(CharacterResource resource) {
		String url = httpURL(((RestDevice) resource.getDevice()).getAddress(), resource.getResource(), null);
		CharacterResponse characterResponse = (CharacterResponse) getByResponseType(url, CharacterResponse.class);
		if (characterResponse != null) {
			resource.setCurrentState(characterResponse.getValue());
		} else {
			resource.setCurrentState(null);
			writeLog(resource.getResource(),"[ " + resource.getName() + " ] - not responding");

		}
		return resource;
	}
	/**
	 * Read single {@link Device} with all fields
	 * @param device
	 * @return
	 */
	public RestDevice readDevice(RestDevice device) {

		String url = httpURL(device.getAddress(), null, null);
		DeviceInfoResponse deviceResponse = aRestConnector.getAsObject(url, DeviceInfoResponse.class);
		if (deviceResponse != null) {
			// System.err.println(deviceResponse);
			Map<String, Object> variables = deviceResponse.getVariables();
			for (DeviceResource deviceResource : device.getResources()) {
				if (variables.containsKey(deviceResource.getResource())) {
					Object object = variables.get(deviceResource.getResource());

					if (deviceResource instanceof CharacterResource || deviceResource instanceof IntegerResource) {
						deviceResource.setCurrentState(object);
					} else if (deviceResource instanceof FloatResource) {
						deviceResource.setCurrentState(((Double) object).floatValue());
					} else if (deviceResource instanceof BooleanResource) {
						deviceResource
								.setCurrentState(object.equals(true) ? ((BooleanResource) deviceResource).getOnLabel()
										: ((BooleanResource) deviceResource).getOffLabel());
					}
				}
			}
		} else {
			for (DeviceResource resource : device.getResources()) {
				resource.setCurrentState(null);
			}
			Event event = new Event();
			event.setEventDate(LocalDateTime.now());
			event.setEventOwner(ARestService.class.getSimpleName());
			event.setEventObject(device.getName());
			event.setEventInfo("[ " + device.getName() + " ] - device not responding");
			eventRepository.checkAndSave(event);
			//writeLog(device.getName(),"[ " + device.getName() + " ] - device not responding");
		}
		return device;
	}
}
