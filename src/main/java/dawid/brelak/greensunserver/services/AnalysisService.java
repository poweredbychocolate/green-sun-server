package dawid.brelak.greensunserver.services;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsage;
import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsageId;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.repositories.MontlyUsageRepository;
/**
 * Service that monitor {@link DeviceResource} usage
 * @author Dawid
 * @since 2018-12-16
 * @version 1
 */
@Service
public class AnalysisService {
	@Autowired
	private MontlyUsageRepository montlyUsageRepository;
	private AtomicInteger usageCounter;
	private ConcurrentHashMap<MonthlyResourceUsageId, MonthlyResourceUsage> usageMap;
	private final Integer bufferSize=20;

	public AnalysisService() {
		usageCounter = new AtomicInteger(0);
		usageMap = new ConcurrentHashMap<>();
	}
	/**
	 * Every hour save usage  
	 */
	@PreDestroy
	@Scheduled(cron = "0 0 0/1 * * *")
	void autoFlush() {
		if (usageMap.size() > 0) {
			flush(new HashMap<>(usageMap));
		}
	}
	/**
	 * Add increment usage and add to list
	 * @param usageDate
	 * @param deviceResource
	 */
	@Async
	public void usedResource(LocalDate usageDate, DeviceResource deviceResource) {
		System.err.println("[Used Resource] : " + usageCounter.get());
		MonthlyResourceUsage resourceUsage = new MonthlyResourceUsage();
		MonthlyResourceUsageId id = new MonthlyResourceUsageId(usageDate.getYear(), usageDate.getMonthValue(),
				deviceResource.getId());
		resourceUsage.setDeviceResource(deviceResource);
		resourceUsage.setResourceId(deviceResource.getId());
		resourceUsage.setUsageCount(1L);
		resourceUsage.setYear(usageDate.getYear());
		resourceUsage.setMonth(usageDate.getMonthValue());
		if (usageCounter.get() >= bufferSize) {
			flush(new HashMap<>(usageMap));
			usageMap.clear();
			usageCounter.set(0);
		}
		usageMap.merge(id, resourceUsage, (oldOld, newNew) -> {
			oldOld.setUsageCount(newNew.getUsageCount() + oldOld.getUsageCount());
			return oldOld;
		});
		usageCounter.incrementAndGet();
	}
	/**
	 * Flush list and save entries 
	 * @param map
	 */
	@Async
	void flush(HashMap<MonthlyResourceUsageId, MonthlyResourceUsage> map) {
		if (map != null&&map.size()>0) {
			System.err.println("[Flush] : " + map.size());
			List<MonthlyResourceUsage> list = montlyUsageRepository.findAllByKeys(map.keySet());
			list.size();
			for (MonthlyResourceUsage mru : list) {
				MonthlyResourceUsageId id = new MonthlyResourceUsageId(mru.getYear(), mru.getMonth(),
						mru.getResourceId());
				map.merge(id, mru, (oldOld, newNew) -> {
					newNew.setUsageCount(newNew.getUsageCount() + oldOld.getUsageCount());
					return newNew;
				});
			}
			for (MonthlyResourceUsageId mId : map.keySet()) {
				try {
					MonthlyResourceUsage usage =map.get(mId);
					montlyUsageRepository.save(usage);
				} catch (Exception e) {
					//map.remove(mId);
				}
			}
			map.clear();
			montlyUsageRepository.notifyDataChange();
		}
	}
}
