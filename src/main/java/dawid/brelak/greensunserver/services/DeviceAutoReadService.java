package dawid.brelak.greensunserver.services;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsage;
import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.repositories.MontlyUsageRepository;
import dawid.brelak.greensunserver.repositories.RestDeviceRepository;

/**
 * Service periodically check device states
 * 
 * @author Dawid
 * @since 2018-12-09
 * @version 1
 */
@Service
public class DeviceAutoReadService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Device> devicesList;
	private ConcurrentHashMap<Integer, DeviceResource> resourcesMap;
	private List<Integer> idKeys;
	@Autowired
	private RestDeviceRepository restDeviceRepository;
	@Autowired
	private ARestService aRestService;
	@Autowired
	private MontlyUsageRepository montlyUsageRepository;
	@Autowired
	private TriggerService triggerService;
	private Long monthlyUsageDataKey;
	private Long devicesDataKey;

	public DeviceAutoReadService() {
		devicesList = new CopyOnWriteArrayList<>();
		resourcesMap = new ConcurrentHashMap<>();
	}

	@PostConstruct
	private void init() {
		devicesList.clear();
		devicesList.addAll(restDeviceRepository.findAll());
		LocalDate localDate = LocalDate.now();
		idKeys = montlyUsageRepository.getMonthlyKeys(localDate.getYear(), localDate.getMonthValue());
		monthlyUsageDataKey = montlyUsageRepository.getDataKey();
		devicesDataKey = restDeviceRepository.getDataKey();
		refreshList();
	}
	/**
	 * Periodically update {@link DeviceResource#setCurrentState(Object)} 
	 */
	@Scheduled(cron = "*/5 * * * * *")
	@Async
	void refreshList() {
		System.err.println("[Refresh List]");
		if (!devicesDataKey.equals(restDeviceRepository.getDataKey())) {
			devicesList.clear();
			devicesList.addAll(restDeviceRepository.findAll());
			devicesDataKey = restDeviceRepository.getDataKey();
			System.err.println("[Devices Data Key] : " + devicesDataKey);
		}
		System.err.println("[Devices : " + devicesList.size() + "]");
		for (Device device : devicesList) {
			if (device instanceof RestDevice) {
				device = aRestService.readDevice((RestDevice) device);
				for (DeviceResource resource : device.getResources()) {
					resourcesMap.merge(resource.getId(), resource, (old, n) -> {
						old.setCurrentState(n.getCurrentState());
						return old;
					});
				}
			}
		}
		printKeys(new ArrayList<>(resourcesMap.values()));
		triggerService.addResource(
				resourcesMap.values().stream().filter(dr -> dr.getReadOnly()).collect(Collectors.toList()));
		System.err.println("[Resources : " + resourcesMap.size() + "]");
	}

	////
	/**
	 * @return the aRestService
	 */
	public ARestService getARestService() {
		return aRestService;
	}

	/**
	 * @return the devicesList
	 */
	public List<Device> getDevicesList() {
		return devicesList;
	}

	/**
	 * @return the resourcesList
	 */
	public List<DeviceResource> getResourcesList() {
		List<DeviceResource> list = resourcesMap.values().stream().collect(Collectors.toList());
		printKeys(list);
		return list;
	}

	/**
	 * @return the resourcesList sorted by readOnly
	 */
	public List<DeviceResource> getResourcesListReadOnlyFirst() {
		List<DeviceResource> list = resourcesMap.values().stream()
				.sorted(Comparator.comparing(DeviceResource::getReadOnly, Comparator.reverseOrder()))
				.collect(Collectors.toList());
//		printKeys(list);
		return list;
	}
	/**
	 * {@link DeviceResource} filtered by {@link DeviceResource#getReadOnly()}
	 * @param readOnly
	 * @return
	 */
	public List<DeviceResource> getResourcesList(Boolean readOnly) {
		List<DeviceResource> list = new ArrayList<>();
		list = resourcesMap.values().stream().filter(resource -> resource.getReadOnly().equals(readOnly))
				.collect(Collectors.toList());
//		printKeys(list);
		return list;
	}
	/**
	 * {@link DeviceResource} list sorted using {@link MonthlyResourceUsage}
	 * @param readOnly
	 * @return
	 */
	public List<DeviceResource> getResourcesListByMonthlyUsage(Boolean readOnly) {
		List<DeviceResource> list = new ArrayList<>();
		list = resourcesMap.values().stream().filter(resource -> resource.getReadOnly().equals(readOnly))
				.collect(Collectors.toList());
		if (!monthlyUsageDataKey.equals(montlyUsageRepository.getDataKey())) {
			LocalDate localDate = LocalDate.now();
			idKeys = montlyUsageRepository.getMonthlyKeys(localDate.getYear(), localDate.getMonthValue());
			monthlyUsageDataKey = montlyUsageRepository.getDataKey();
			System.err.println("[MONTHLY KEY] : " + monthlyUsageDataKey);
		}
		if (idKeys != null && idKeys.size() > 0) {
			List<DeviceResource> sorterdList = new ArrayList<>();
			for (Integer key : idKeys) {
				if (resourcesMap.containsKey(key)) {
					DeviceResource tmp = resourcesMap.get(key);
					sorterdList.add(tmp);
					list.remove(tmp);
				}
			}
			sorterdList.addAll(list);
			list.clear();
			list = sorterdList;
		}
		System.err.println("[By Monthly Usage]");
		printKeys(list);
		return list;
	}
	/**
	 * replace device 
	 * @param deviceResource
	 */
	@Async
	public void replaceResource(DeviceResource deviceResource) {
		if (resourcesMap.containsKey(deviceResource.getId())) {
			resourcesMap.replace(deviceResource.getId(), deviceResource);
			triggerService.addResource(deviceResource);
		}
	}
	/**
	 * Remove devices from list using {@link DeviceResource#getId()}
	 * @param ids
	 */
	@Async
	public void removeResource(List<Integer> ids) {
		for (Integer integer : ids) {
			if (resourcesMap.containsKey(integer)) {
				resourcesMap.remove(integer);
			}
		}
	}

	private void printKeys(List<DeviceResource> resources) {
		if (resources != null && resources.size() > 1) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("[");
			for (int i = 0; i < resources.size() - 1; i++) {
				stringBuilder.append(resources.get(i).getId());
				stringBuilder.append(", ");
			}
			stringBuilder.append(resources.get(resources.size() - 1).getId());
			stringBuilder.append("]");
			System.err.println(stringBuilder.toString());
		}
	}

}
