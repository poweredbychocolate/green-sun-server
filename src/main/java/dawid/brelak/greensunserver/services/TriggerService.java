package dawid.brelak.greensunserver.services;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;
import dawid.brelak.greensunserver.model.trigger.ResourceTrigger;
import dawid.brelak.greensunserver.model.trigger.StandardTrigger;
import dawid.brelak.greensunserver.model.trigger.TriggerAction;
import dawid.brelak.greensunserver.repositories.TriggerRepository;

/**
 * Service that allow call {@link ResourceTrigger}
 * 
 * @author Dawid
 * @since 2018-12-28
 * @version 1
 */
@Service
public class TriggerService implements Serializable {
	private static final long serialVersionUID = 1L;
	private List<ResourceTrigger> triggers;
	private Queue<DeviceResource> deviceResourcesQueue;
	// key = resource id , value=last used trigger
	// for block call again trigger
	private ConcurrentHashMap<Integer, ResourceTrigger> lockedTriggers;
	// trigger types that should be called only one
	private List<String> blockingRT;
	private Long triggersKey;
	private AtomicBoolean monitorOnline;
	private ExecutorService executorService;
	@Autowired
	private TriggerRepository triggerRepository;
	@Autowired
	private ARestService aRestService;
	@Autowired
	@Lazy
	private DeviceAutoReadService deviceAutoReadService;

	public TriggerService() {
		System.err.println("[Starting Trigger Service]");
		this.triggers = new CopyOnWriteArrayList<>();
		this.deviceResourcesQueue = new ConcurrentLinkedQueue<>();
		this.monitorOnline = new AtomicBoolean(true);
		this.executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		this.lockedTriggers = new ConcurrentHashMap<>();
		this.blockingRT = new CopyOnWriteArrayList<>(Arrays.asList(StandardTrigger.TYPE_EQUALS_SINGLE,
				StandardTrigger.TYPE_ABOVE_SINGLE, StandardTrigger.TYPE_BELOW_SINGLE));
	}

	@PostConstruct
	private void init() {
		triggers.addAll(triggerRepository.findAll());
		triggersKey = triggerRepository.getDataKey();
		System.err.println("[Cores]" + Runtime.getRuntime().availableProcessors());
		executorService.execute(new QueueMonitor());
		System.err.println("[Started Trigger Service]");
	}

	@PreDestroy
	private void onFinish() {
		monitorOnline.set(false);
		executorService.shutdown();
		System.err.println("[Stop Trigger Service]");
	}

	/**
	 * Placed single {@link DeviceResource} in {@link ConcurrentLinkedQueue}
	 * 
	 * @param deviceResource
	 */
	@Async
	public void addResource(DeviceResource deviceResource) {
		//System.err.println("[DeviceResource Queue] : "+deviceResource);
		deviceResourcesQueue.offer(deviceResource);
	}

	/**
	 * Placed list {@link DeviceResource} in {@link ConcurrentLinkedQueue}
	 * 
	 * @param deviceResources
	 */
	@Async
	public void addResource(List<DeviceResource> deviceResources) {
		for (DeviceResource deviceResource : deviceResources) {
			deviceResourcesQueue.offer(deviceResource);
		}
	}

	/**
	 * Monitor queue, load,check and call triggers
	 * 
	 * @author Dawid
	 * @since 2018-12-28
	 * @version 1
	 *
	 */
	class QueueMonitor extends Thread {
		@Override
		public void run() {
			System.err.println("[Start Queue Monitor]");
			while (monitorOnline.get()) {
				if (!triggersKey.equals(triggerRepository.getDataKey())) {
					triggers.clear();
					triggers.addAll(triggerRepository.findAll());
					triggersKey = triggerRepository.getDataKey();
					System.err.println("[Reload Triggers List]");
				}
				if (!deviceResourcesQueue.isEmpty() && !triggers.isEmpty()) {
					DeviceResource deviceResource = deviceResourcesQueue.poll();

					List<ResourceTrigger> resourceTriggers = triggers.stream()
							.filter(rt -> rt.getSourceResource().getId().equals(deviceResource.getId()))
							.collect(Collectors.toList());

					if (resourceTriggers != null) {
						for (ResourceTrigger resourceTrigger : resourceTriggers) {
							boolean switchTrigger = false;
							if (resourceTrigger instanceof StandardTrigger) {
								switchTrigger = compareValues(deviceResource, resourceTrigger.getSourceValue(),
										((StandardTrigger) resourceTrigger).getTriggerType());
							}
							if (switchTrigger) {
								if (resourceTrigger instanceof StandardTrigger
										&& blockingRT.contains(((StandardTrigger) resourceTrigger).getTriggerType())) {
									if (!lockedTriggers.containsKey(deviceResource.getId())) {
										System.err.println("[Trigger Call]" + resourceTrigger);
										executorService
												.execute(new QueueMonitorWriter(resourceTrigger.getTriggerActions()));
										lockedTriggers.put(deviceResource.getId(), resourceTrigger);
									} else if (!lockedTriggers.get(deviceResource.getId()).equals(resourceTrigger)) {
										System.err.println("[Trigger Call]" + resourceTrigger);
										executorService
												.execute(new QueueMonitorWriter(resourceTrigger.getTriggerActions()));
										lockedTriggers.replace(deviceResource.getId(), resourceTrigger);
									}
								} else {
									System.err.println("[Trigger Call]" + resourceTrigger);
									executorService
											.execute(new QueueMonitorWriter(resourceTrigger.getTriggerActions()));
								}
							}
						}
					}
				}
			}
			System.err.println("[Stop Queue Monitor]");
		}
	}

	/**
	 * Check if call triggers actions
	 * 
	 * @param resource
	 * @param value
	 * @param type
	 * @return
	 */
	private boolean compareValues(DeviceResource resource, String value, String type) {
		if (resource == null || value == null || type == null)
			return false;
		if (resource != null && resource.getCurrentState() == null)
			return false;

		if (resource instanceof BooleanResource) {
			if ((type.equals(StandardTrigger.TYPE_EQUALS_SINGLE) || type.equals(StandardTrigger.TYPE_EQUALS_REPEAT))
					&& resource.getCurrentState().equals(value)) {
				return true;
			}
		} else if (resource instanceof IntegerResource) {
			try {
				int iValue = Integer.parseInt(value);
				if ((type.equals(StandardTrigger.TYPE_EQUALS_SINGLE) || type.equals(StandardTrigger.TYPE_EQUALS_REPEAT))
						&& resource.getCurrentState().equals(iValue)) {
					return true;
				} else if ((type.equals(StandardTrigger.TYPE_BELOW_SINGLE)
						|| type.equals(StandardTrigger.TYPE_BELOW_REPEAT))
						&& ((Integer) resource.getCurrentState()) < iValue) {
					return true;
				} else if ((type.equals(StandardTrigger.TYPE_ABOVE_SINGLE)
						|| type.equals(StandardTrigger.TYPE_ABOVE_REPEAT))
						&& ((Integer) resource.getCurrentState()) > iValue) {
					return true;
				}
			} catch (Exception e) {

			}
		} else if (resource instanceof CharacterResource) {
			if ((type.equals(StandardTrigger.TYPE_EQUALS_SINGLE) || type.equals(StandardTrigger.TYPE_EQUALS_REPEAT))
					&& resource.getCurrentState().equals(value)) {
				return true;
			}
		} else if (resource instanceof FloatResource) {
			try {
				float fValue = Float.parseFloat(value);
				if ((type.equals(StandardTrigger.TYPE_EQUALS_SINGLE) || type.equals(StandardTrigger.TYPE_EQUALS_REPEAT))
						&& resource.getCurrentState().equals(fValue)) {
					return true;
				} else if ((type.equals(StandardTrigger.TYPE_BELOW_SINGLE)
						|| type.equals(StandardTrigger.TYPE_BELOW_REPEAT))
						&& ((Float) resource.getCurrentState()) < fValue) {
					return true;
				} else if ((type.equals(StandardTrigger.TYPE_ABOVE_SINGLE)
						|| type.equals(StandardTrigger.TYPE_BELOW_REPEAT))
						&& ((Float) resource.getCurrentState()) > fValue) {
					return true;
				}
			} catch (Exception e) {

			}
		}
		return false;
	}

	/**
	 * In new thread set {@link DeviceResource#setCurrentState(Object)}
	 * @author Dawid
	 * @since 2018-12-28
	 * @version 1
	 */
	class QueueMonitorWriter extends Thread {
		private List<TriggerAction> actions;

		public QueueMonitorWriter(List<TriggerAction> actions) {
			super();
			this.actions = actions;
		}

		@Override
		public void run() {
			for (TriggerAction action : actions) {
				DeviceResource responseResource = null;
				DeviceResource resource = action.getResource();
				String value = action.getValue();
				try {
					if (resource instanceof BooleanResource) {
						responseResource = aRestService.writeResource((BooleanResource) resource, value);
					} else if (resource instanceof IntegerResource) {
						responseResource = aRestService.writeResource((IntegerResource) resource,
								Integer.parseInt(value));
					} else if (resource instanceof CharacterResource) {
						responseResource = aRestService.writeResource((CharacterResource) resource, value);
					} else if (resource instanceof FloatResource) {
						responseResource = aRestService.writeResource((FloatResource) resource,
								Float.parseFloat(value));
					}
				} catch (Exception e) {
				}
				deviceAutoReadService.replaceResource(responseResource);
				System.err.println("[Queue Monitor Writer]" + responseResource.getName() + " : "
						+ responseResource.getCurrentState());
			}
		}

	}
}
