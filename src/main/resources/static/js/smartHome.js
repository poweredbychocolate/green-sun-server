/**
 * 
 */
function redirectWithPathVariable(pageName, pathVariable) {
	window.location.href = pageName + "/" + pathVariable;
}

function pageRedirect(pageName) {
	window.location.href = pageName;
}
function callResource(url) {
	window.location.href = url;
	return 0;
}
function openDialog(dialogName) {
	var dialog = document.getElementById(dialogName);
	dialog.showModal();
}

function closeDialog(dialogName) {
	var dialog = document.getElementById(dialogName);
	dialog.close();
}
function submitForm(formName) {
	alert(formName);
	var dialog = document.getElementById(formName);
	dialog.submit();
}
function showCircle() {
	  document.getElementById("circle").className = 'rotary-circle';
	}
