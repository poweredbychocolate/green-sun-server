package dawid.brelak.greensunserver.connectors;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import dawid.brelak.greensunserver.connectors.ARestConnector;
import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;

/**
 * 
 * @author Dawid
 * @since 2018-11-02
 * @version 2
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ARestConnectorTest {

	private static final String testDevice = "http://192.168.0.60/";
	private static final String testResource = "http://192.168.0.60/integer";
	private static final String bad = "http://192.168.0.50/";
	
	@MockBean
	RestTemplate restTemplate;
	@Autowired
	ARestConnector restConnectorService;
	RestDevice device;
	
	@BeforeEach
	void init() {
//		restConnectorService = new ARestConnector(restTemplate);
		 device = new RestDevice();
		device.setDescription("JUNIT TEST");
		device.setName("JUNIT DEVICE");
		device.setAddress("127.0.0.1");
		CharacterResource characterResource = new CharacterResource("Character", "character", device, null, 6);
		BooleanResource booleanResource = new BooleanResource("Boolean", "coolean", device, null, "ON", "OFF");
		IntegerResource integerResource = new IntegerResource("Integer", "integer", device, null, 0, 100);
		FloatResource floatResource = new FloatResource("Float", "float", device, null, 0f,100f);
		device.setResources(Arrays.asList(characterResource,booleanResource,integerResource,floatResource));
		
		when(restTemplate.getForEntity(bad, String.class)).thenReturn(null);
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("{\"variables\": {" );
		stringBuilder.append("\""+characterResource.getResource()+"\": "+"TEST");
		stringBuilder.append("\""+booleanResource.getResource()+"\": "+booleanResource.getOnLabel());
		stringBuilder.append("\""+integerResource.getResource()+"\": "+25);
		stringBuilder.append("\""+floatResource.getResource()+"\": "+98.25f);
		stringBuilder.append("}, ");
		stringBuilder.append("\"id\": \"111\", ");
		stringBuilder.append("\"name\": "+device.getName()+", ");
		stringBuilder.append(" \"hardware\": \"junit\", \"connected\": "+true+"}");
		ResponseEntity<String> stringEntity = new ResponseEntity<String>(stringBuilder.toString(),HttpStatus.OK);
		when(restTemplate.getForEntity(testDevice, String.class)).thenReturn(stringEntity);
		
		ResponseEntity<RestDevice> restEntity = new ResponseEntity<RestDevice>(device,HttpStatus.OK);
		when(restTemplate.getForEntity(testDevice, RestDevice.class)).thenReturn(restEntity);
		
		stringBuilder = new StringBuilder();
		stringBuilder.append("{\""+integerResource.getResource()+"\": "+25+" ,");
		stringBuilder.append("\"id\": \"111\", ");
		stringBuilder.append("\"name\": "+device.getName()+", ");
		stringBuilder.append(" \"hardware\": \"junit\", \"connected\": "+true+"}");
		stringEntity = new ResponseEntity<String>(stringBuilder.toString(),HttpStatus.OK);
		when(restTemplate.getForEntity(testResource, String.class)).thenReturn(stringEntity);
	
	}
	
	@Test
	void connectorTest() {
		
		assertNotNull(restConnectorService);
		String response = restConnectorService.getAsString(bad);
		assertNull(response);
		
		response = restConnectorService.getAsString(testDevice);
		System.err.println(response);
		assertNotNull(response);
		
		response = restConnectorService.getAsString(testResource);
		assertNotNull(response);
		List<String> splitList = restConnectorService.splitResponse(response);
		System.err.println(splitList);
		assertNotNull(splitList);
		assertEquals(10, splitList.size());
		
		RestDevice device2 = restConnectorService.getAsObject(testDevice, RestDevice.class);
		System.err.println(device2);
		assertNotNull(device2);
		assertEquals(device2, device);

	}
}
