package dawid.brelak.greensunserver.connectors;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;

/**
 * 
 * @author Dawid
 * @since 2018-12-23
 * @version 1
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class DeviceConfigFileConnectorTest {
	@Autowired
	DeviceConfigFileConnector fileConnector;
	@Test
	void loadFromCSVTest() {
		assertNotNull(fileConnector);
		Path path =Paths.get("Exaple.csv");
		Device exapleDevice =new RestDevice("Exampe Module","XXX.WWW.XXX.CCC","Exaple Description");
		exapleDevice.getResources().add(new BooleanResource("Boolean Exaple", "bexaple", exapleDevice, null, "Exaple ON", "Exaple OFF"));
		IntegerResource iR=new IntegerResource("Integer Exaple", "iResource", exapleDevice, null, 0, 100);
		iR.setReadOnly(true);
		exapleDevice.getResources().add(iR);
		FloatResource fR = new FloatResource("Flaot Example", "fResource", exapleDevice, null, 10.0f, 155.5f);
		fR.setReadOnly(true);
		exapleDevice.getResources().add(fR);
		exapleDevice.getResources().add(new CharacterResource("Character Exaple", "cgResource", exapleDevice, null, 100));
		
		fileConnector.exportToFile(exapleDevice, path);
		assertNull(fileConnector.loadFromCSVFile(Paths.get("Broken.csv")));
		Device device=fileConnector.loadFromCSVFile(path);
		assertNotNull(device);
		assertNotNull(device.getResources());
		assertEquals(4, device.getResources().size());
		assertNotNull(device.getDescription());
		assertNotNull(device.getName());
		try {
			assertTrue(Files.deleteIfExists(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
