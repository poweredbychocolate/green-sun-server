package dawid.brelak.greensunserver.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

import dawid.brelak.greensunserver.model.arest.BooleanResponse;
import dawid.brelak.greensunserver.model.arest.CharacterResponse;
import dawid.brelak.greensunserver.model.arest.DeviceInfoResponse;
import dawid.brelak.greensunserver.model.arest.FloatResponse;
import dawid.brelak.greensunserver.model.arest.IntegerResponse;

/**
 * 
 * @author Dawid
 * @since 2019-01-19
 * @version 1.0
 */
public class ARestTest {

	@Test
	public void aRestTest() {
		
		DeviceInfoResponse deviceInfoResponse = new DeviceInfoResponse();
		deviceInfoResponse.setConnected(true);
		deviceInfoResponse.setHardware("JUNIT");
		deviceInfoResponse.setId(1);
		deviceInfoResponse.setName("JUNIT deviceInfoResponse");
		Map<String, Object> map= new HashMap<>();
		map.put("Test", 10);
		map.put("test2", "Test value");
		map.put("test3", true);
		deviceInfoResponse.setVariables(map);
		assertNotEquals(new Integer(0), deviceInfoResponse.getId());
		assertTrue(deviceInfoResponse.getConnected());
		assertNotNull(deviceInfoResponse.getHardware());
		assertNotNull(deviceInfoResponse.getName());
		assertNotNull(deviceInfoResponse.getVariables());
		assertEquals(3, deviceInfoResponse.getVariables().size());
		assertNotNull(deviceInfoResponse.toString());
		////
		IntegerResponse integerResponse = new IntegerResponse();
		integerResponse.setConnected(false);
		integerResponse.setHardware("JUNIT");
		integerResponse.setId(2);
		integerResponse.setName("JUNIT IntegerResponse");
		assertEquals(new Integer(2), integerResponse.getId());
		assertFalse(integerResponse.getConnected());
		assertNotNull(integerResponse.getHardware());
		assertNotNull(integerResponse.getName());
		assertNotNull(integerResponse.toString());
		integerResponse.setValue(100);
		integerResponse.setValueName("Integer Value");
		assertEquals(new Integer(100), integerResponse.getValue());
		assertNotNull(integerResponse.getValueName());
		////
		FloatResponse floatResponse = new FloatResponse();
		floatResponse.setConnected(true);
		floatResponse.setHardware("JUNIT");
		floatResponse.setId(3);
		floatResponse.setName("JUNIT FloatResponse");
		assertEquals(new Integer(3), floatResponse.getId());
		assertTrue(floatResponse.getConnected());
		assertNotNull(floatResponse.getHardware());
		assertNotNull(floatResponse.getName());
		assertNotNull(floatResponse.toString());
		floatResponse.setValue(100f);
		floatResponse.setValueName("Flaot Value");
		assertEquals(new Float(100), floatResponse.getValue());
		assertNotNull(floatResponse.getValueName());
		////
		BooleanResponse booleanResponse = new BooleanResponse();
		booleanResponse.setConnected(true);
		booleanResponse.setHardware("JUNIT");
		booleanResponse.setId(4);
		booleanResponse.setName("JUNIT BooleanResponse");
		assertNotEquals(new Integer(0), booleanResponse.getId());
		assertTrue(booleanResponse.getConnected());
		assertNotNull(booleanResponse.getHardware());
		assertNotNull(booleanResponse.getName());
		assertNotNull(booleanResponse.toString());
		booleanResponse.setValue(true);
		booleanResponse.setValueName("Boolean Value");
		assertEquals(true, booleanResponse.getValue());
		assertNotNull(booleanResponse.getValueName());
		
		////
		CharacterResponse characterResponse = new CharacterResponse();
		characterResponse.setConnected(false);
		characterResponse.setHardware("JUNIT");
		characterResponse.setId(5);
		characterResponse.setName("JUNIT CharacterResponse");
		assertEquals(new Integer(5), characterResponse.getId());
		assertFalse(characterResponse.getConnected());
		assertNotNull(characterResponse.getHardware());
		assertNotNull(characterResponse.getName());
		assertNotNull(characterResponse.toString());
		characterResponse.setValue("Character");
		characterResponse.setValueName("Character Value");
		assertEquals("Character", characterResponse.getValue());
		assertNotNull(characterResponse.getValueName());
		
	}

}
