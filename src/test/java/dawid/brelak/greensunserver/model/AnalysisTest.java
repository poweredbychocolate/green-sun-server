package dawid.brelak.greensunserver.model;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import dawid.brelak.greensunserver.model.analysis.Event;
import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsage;
import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsageId;
import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;

/**
 * 
 * @author Dawid
 * @since 2019-01-19
 * @version 1.0
 */
public class AnalysisTest {

	@Test
	public void eventTest() {

		Event event1 = new Event(LocalDateTime.now(), "JUNIT", "Event1", "Unknown event 1");
		assertNull(event1.getId());
		assertNotNull(event1.getEventDate());
		assertNotNull(event1.getEventOwner());
		assertNotNull(event1.getEventObject());
		assertNotNull(event1.getEventInfo());

		Event event2 = new Event();
		event2.setEventDate(LocalDateTime.now().minusHours(3));
		event2.setEventInfo("Unknow event 2");
		event2.setEventObject("Event2");
		event2.setEventOwner("JUNIT");
		event2.setId(10L);
		assertEquals(new Long(10), event2.getId());
		assertNotNull(event2.getEventDate());
		assertNotNull(event2.getEventOwner());
		assertNotNull(event2.getEventObject());
		assertNotNull(event2.getEventInfo());

		assertNotEquals(event1.toString(), event2.toString());

	}

	@Test
	public void monthlyResourceUsageIdTest() {
		LocalDateTime time = LocalDateTime.now();
		MonthlyResourceUsageId usageId1 = new MonthlyResourceUsageId(time.getYear(), time.getMonthValue(), 1);
		MonthlyResourceUsageId usageId2 = new MonthlyResourceUsageId();
		time = LocalDateTime.now();
		usageId2.setMonth(time.getMonthValue());
		usageId2.setResourceId(2);
		usageId2.setYear(time.getYear());

		assertNotNull(usageId1.getMonth());
		assertNotNull(usageId1.getResourceId());
		assertNotNull(usageId1.getYear());
		assertNotNull(usageId1.toString());
		assertNotNull(usageId2.getMonth());
		assertNotNull(usageId2.getResourceId());
		assertNotNull(usageId2.getYear());
		assertNotNull(usageId2.toString());
		assertEquals(usageId1.getYear(), usageId2.getYear());
		assertEquals(usageId1.getMonth(), usageId2.getMonth());
		assertNotEquals(usageId1.getResourceId(), usageId2.getResourceId());
		assertNotEquals(usageId1.toString(), usageId2.toString());
		assertTrue(usageId1.equals(usageId1));
		assertFalse(usageId1.equals(null));
		assertFalse(usageId2.equals(new Object()));
		assertFalse(usageId2.equals(usageId1));
		usageId1.setResourceId(2);
		assertTrue(usageId1.equals(usageId2));
	}

	@Test
	public void monthlyResourceUsageTest() {
		LocalDateTime time = LocalDateTime.now();
		Device device = new RestDevice();
		BooleanResource booleanResource = new BooleanResource("Boolean", "Boolean", device, null, "ON", "OFF");
		booleanResource.setId(1);
		IntegerResource integerResource = new IntegerResource("Integer", "Integer", device, null, 0, 100);
		integerResource.setId(2);
		MonthlyResourceUsage usage1 = new MonthlyResourceUsage(time.getYear(), time.getMonthValue(), booleanResource.getId(), 20l,
				booleanResource);
		MonthlyResourceUsage usage2 = new MonthlyResourceUsage();
		time = LocalDateTime.now();
		usage2.setMonth(time.getMonthValue());
		usage2.setResourceId(integerResource.getId());
		usage2.setYear(time.getYear());
		usage2.setUsageCount(40l);
		usage2.setDeviceResource(integerResource);

		assertNotNull(usage1.getMonth());
		assertNotNull(usage1.getResourceId());
		assertNotNull(usage1.getYear());
		assertNotNull(usage1.getDeviceResource());
		assertNotNull(usage1.getUsageCount());
		assertNotNull(usage1.toString());
		assertNotNull(usage2.getMonth());
		assertNotNull(usage2.getResourceId());
		assertNotNull(usage2.getYear());
		assertNotNull(usage2.getDeviceResource());
		assertNotNull(usage2.getUsageCount());
		assertNotNull(usage2.toString());
		assertEquals(usage1.getYear(), usage2.getYear());
		assertEquals(usage1.getMonth(), usage2.getMonth());
		assertNotEquals(usage1.getResourceId(), usage2.getResourceId());
		assertNotEquals(usage1.toString(), usage2.toString());
		assertTrue(usage1.equals(usage1));
		assertFalse(usage1.equals(null));
		assertFalse(usage2.equals(new Object()));
		assertFalse(usage2.equals(usage1));
		usage1.setResourceId(integerResource.getId());
		usage1.setDeviceResource(integerResource);
		usage1.setUsageCount(100l);
		usage2.setUsageCount(100l);
		assertTrue(usage1.equals(usage2));

	}

}
