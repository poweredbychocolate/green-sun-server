package dawid.brelak.greensunserver.model;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;

/**
 * 
 * @author Dawid
 * @since 2018-11-01
 * @version 1.0
 */
public class DeviceResourceTest {

	@Test
	public void deviceResourceTest() {
		Device device = new RestDevice();
		String characterState = "1234";
		String onValue = "PowerON";
		String offValue = "PowerOFF";
		Integer minValue = 0;
		Integer maxValue = 200;
		////
		CharacterResource characterPin = new CharacterResource();
		characterPin.setDevice(device);
		characterPin.setMaxLength(6);
		characterPin.setResource("Character");
		BooleanResource characterSwitchPin = new BooleanResource("BooleanResourceTest","Boolean", device, null, onValue,
				offValue);
		IntegerResource integerPin = new IntegerResource("Integer","Integer", device, null, minValue, maxValue);
		FloatResource floatingPointPin = new FloatResource("FloatResourceTest","Float", device, null, (float) minValue,
				(float) maxValue);
		////
		assertFalse(characterPin.setCurrentState("123456789"));
		assertFalse( characterSwitchPin.setCurrentState("OFF"));
		assertFalse(integerPin.setCurrentState(201));
		assertFalse(integerPin.setCurrentState(-1));
		assertFalse(floatingPointPin.setCurrentState(200.01f));
		assertFalse(floatingPointPin.setCurrentState(-0.999f));
		////
		assertDoesNotThrow(() -> characterPin.setCurrentState(characterState));
		assertEquals(characterState.length(), ((String) characterPin.getCurrentState()).length());
		assertDoesNotThrow(() -> characterPin.setCurrentState("123"));
		assertTrue((((String) characterPin.getCurrentState()).length() <= 6));

		assertDoesNotThrow(() -> characterSwitchPin.setCurrentState(onValue));
		assertEquals(onValue, characterSwitchPin.getCurrentState());
		assertNotEquals(offValue, characterSwitchPin.getCurrentState());

		assertDoesNotThrow(() -> integerPin.setCurrentState(0));
		assertEquals(minValue, integerPin.getCurrentState());
		assertNotEquals(maxValue, integerPin.getCurrentState());
		assertDoesNotThrow(() -> integerPin.setCurrentState(200));
		assertEquals(maxValue, integerPin.getCurrentState());
		assertNotEquals(minValue, integerPin.getCurrentState());

		assertDoesNotThrow(() -> floatingPointPin.setCurrentState(0.0f));
		assertEquals(new Float(minValue), floatingPointPin.getCurrentState());
		assertNotEquals(new Float(maxValue), floatingPointPin.getCurrentState());
		assertDoesNotThrow(() -> floatingPointPin.setCurrentState(200.0f));
		assertEquals(new Float(maxValue), floatingPointPin.getCurrentState());
		assertNotEquals(new Float(minValue), floatingPointPin.getCurrentState());

	}

}
