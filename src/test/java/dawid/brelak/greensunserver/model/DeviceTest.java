package dawid.brelak.greensunserver.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.jupiter.api.Test;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;

/**
 * 
 * @author Dawid
 * @since 2018-11-01
 * @version 1.0
 */
public class DeviceTest {

	@Test
	void restDeviceTest() {
		Device device = new RestDevice("RestDeviceTest", "localhost", "Device Test");
		// String characterState = "1234";
		String onValue = "PowerON";
		String offValue = "PowerOFF";
		Integer minValue = 0;
		Integer maxValue = 200;
		////
		CharacterResource characterResource = new CharacterResource("CharacterResourceTest", null, device, null, 6);
		BooleanResource characterSwitchResource= new BooleanResource("BooleanResourceTest", "Boolean", device, null,
				onValue, offValue);
		IntegerResource integerResource = new IntegerResource("IntegerPinTest", "Integer", device, null, minValue, maxValue);
		FloatResource floatingPointResource = new FloatResource("FloatResourceTest", "Float", device, null, (float) minValue,
				(float) maxValue);
		////
		assertNotNull(device.getResources());
		assertTrue(device.getResources().isEmpty());
		device.getResources().add(characterResource);
		device.getResources().add(characterSwitchResource);
		device.getResources().add(integerResource);
		device.getResources().add(floatingPointResource);
		assertNotNull(device.getResources());
		assertFalse(device.getResources().isEmpty());
		assertTrue(device.equals(device));
		assertFalse(device.equals(new RestDevice("Test", "localhost", "Device HE HE")));
		assertFalse(device.equals(new Object()));
		assertFalse(device.equals(null));
	}

}
