package dawid.brelak.greensunserver.model;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.Device;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;
import dawid.brelak.greensunserver.model.trigger.StandardTrigger;
import dawid.brelak.greensunserver.model.trigger.TriggerAction;

/**
 * 
 * @author Dawid
 * @since 2019-01-19
 * @version 1.0
 */
public class TriggersTest {

	@Test
	public void triggerTest() {
		Device device1 = new RestDevice();
		Device device2 = new RestDevice();
		
		device1.setId(1);
		device1.setName("DEVICE 1");
		device1.setDescription("Device 1 Description");
		CharacterResource character1 = new CharacterResource("CharacterTest1",null, device1, null, 20);
		BooleanResource boolean1 = new BooleanResource("BooleanTest1",null, device1, null, "POWER ON",
				"POWER OFF");
		IntegerResource integer1 = new IntegerResource("IntegerTest1",null, device1, null, 0,200);
		FloatResource float1 = new FloatResource("Float1Test1",null, device1, null, (float) 0.0f,
				200.0f);
		device1.getResources().add(character1);
		device1.getResources().add(boolean1);
		device1.getResources().add(integer1);
		device1.getResources().add(float1);
		
		device2.setId(2);
		device2.setName("DEVICE 2");
		device2.setDescription("Device 2 Description");
		CharacterResource character2 = new CharacterResource("CharacterTest2","Character2", device2, null, 20);
		BooleanResource boolean2 = new BooleanResource("BooleanTest2","Boolean2", device2, null, "POWER ON",
				"POWER OFF");
		IntegerResource integer2 = new IntegerResource("IntegerTest2","Integer2", device2, 25, 0,200);
		FloatResource float2 = new FloatResource("Float1Test2","Float2", device2, null, (float) 0.0f,
				200.0f);
		device2.getResources().add(character2);
		device2.getResources().add(boolean2);
		device2.getResources().add(integer2);
		device2.getResources().add(float2);
		
		TriggerAction triggerAction1 = new TriggerAction();
		triggerAction1.setId(1l);
		triggerAction1.setResource(integer2);
		triggerAction1.setValue("20");
		assertEquals("20", triggerAction1.getValue());
		assertNotNull(triggerAction1.getResource());
		assertSame(integer2, triggerAction1.getResource());
		assertEquals(25, triggerAction1.getResource().getCurrentState());
		assertNotEquals(new Long(0), triggerAction1.getId());
		assertNotNull(triggerAction1.toString());
		
		StandardTrigger standardTrigger = new StandardTrigger();
		standardTrigger.setId(1l);
		standardTrigger.setSourceResource(integer1);
		standardTrigger.setSourceValue("30");
		standardTrigger.setTriggerType(StandardTrigger.TYPE_ABOVE_SINGLE);
		standardTrigger.setTriggerActions(new ArrayList<>());
		assertNotNull(standardTrigger.getTriggerActions());
		standardTrigger.getTriggerActions().add(triggerAction1);
		assertSame(integer2, standardTrigger.getTriggerActions().get(0).getResource());
		assertNotEquals(StandardTrigger.TYPE_ABOVE_REPEAT, standardTrigger.getTriggerType());
		assertEquals(StandardTrigger.TYPE_ABOVE_SINGLE, standardTrigger.getTriggerType());
		assertNotEquals(new Long(0), standardTrigger.getId());
		assertNotNull(standardTrigger.toString());
		assertNotNull(StandardTrigger.getTypes());


	}

}
