package dawid.brelak.greensunserver.repositories;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;
/**
 * 
 * @author Dawid
 * @since 2018-11-02
 * @version 1.0
 */
@ExtendWith(SpringExtension.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@SpringBootTest
@DataJpaTest
public class DeviceRepositoryTest {
	@Autowired
	RestDeviceRepository restDeviceRepository;

	@Test
	void restDeviceRepositoryTest() {
		restDeviceRepository.deleteAll();
		RestDevice device = new RestDevice("RestDeviceTest", "localhost", "Device Test");
		String onValue = "PowerON";
		String offValue = "PowerOFF";
		Integer minValue = 0;
		Integer maxValue = 200;
		////
		CharacterResource characterResource = new CharacterResource("CharacterResourceTest", "Character", device, null, 6);
		BooleanResource characterSwitchResource= new BooleanResource("BooleanResourceTest", "Boolean", device, null,
				onValue, offValue);
		IntegerResource integerResource = new IntegerResource("IntegerPinTest", "Integer", device, null, minValue, maxValue);
		FloatResource floatingPointResource = new FloatResource("FloatResourceTest", "Float", device, null, (float) minValue,
				(float) maxValue);
		////
		device.getResources().add(characterResource);
		device.getResources().add(characterSwitchResource);
		device.getResources().add(integerResource);
		device.getResources().add(floatingPointResource);
		////	
		Long key1 =restDeviceRepository.getDataKey();
		assertNotNull(restDeviceRepository);
		assertEquals(key1, restDeviceRepository.getDataKey());
		assertNotNull(restDeviceRepository.save(device));
		restDeviceRepository.notifyDataChange();
		assertNotEquals(key1, restDeviceRepository.getDataKey());
		key1 =restDeviceRepository.getDataKey();
		assertFalse(restDeviceRepository.findAll().isEmpty());
		assertEquals(device, restDeviceRepository.findByName(device.getName()));
		assertNull(restDeviceRepository.findByName("Unknown"));
		assertNotEquals(device, restDeviceRepository.findByName("Unknown"));
		assertEquals(key1, restDeviceRepository.getDataKey());
		restDeviceRepository.deleteAll();
	}

}
