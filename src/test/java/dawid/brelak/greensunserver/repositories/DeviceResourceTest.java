package dawid.brelak.greensunserver.repositories;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.basic.DeviceResource;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;


/**
 * 
 * @author Dawid
 * @since 2019-01-19
 * @version 1
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class DeviceResourceTest {

	@Autowired
	private DeviceResourceRepository repository;
	@Autowired
	private RestDeviceRepository deviceRepository;

	@Test
	public void repositoryTest() {
		RestDevice device = new RestDevice();
		device.setDescription("JUNIT TEST");
		device.setName("JUNIT DEVICE");
		device.setAddress("127.0.0.1");
		IntegerResource integerResource =(IntegerResource) repository.newByName(IntegerResource.class.getSimpleName());
		integerResource.setDevice(device);
		integerResource.setMaxValue(100);
		integerResource.setMinValue(0);
		integerResource.setResource("integer");
		integerResource.setReadOnly(true);
		FloatResource floatResource = (FloatResource) repository.newByName(FloatResource.class.getSimpleName());
		floatResource.setDevice(device);
		floatResource.setMaxValue(100f);
		floatResource.setMinValue(0f);
		floatResource.setResource("float");
		floatResource.setReadOnly(true);
		CharacterResource characterResource =(CharacterResource) repository.newByName(CharacterResource.class.getSimpleName());
		characterResource.setDevice(device);
		characterResource.setMaxLength(20);
		characterResource.setResource("character");
		BooleanResource booleanResource = (BooleanResource) repository.newByName(BooleanResource.class.getSimpleName());
		booleanResource.setDevice(device);
		booleanResource.setOffLabel("POWER OFF");
		booleanResource.setOnLabel("POWER ON");
		booleanResource.setResource("boolean");
		DeviceResource deviceResource =repository.newByName(DeviceResource.class.getSimpleName());
		
		assertNotNull(repository);
		assertNotNull(integerResource);
		assertNotNull(floatResource);
		assertNotNull(characterResource);
		assertNotNull(booleanResource);
		assertNull(deviceResource);
		assertNotNull(deviceRepository.save(device));
		assertNotNull(repository.save(integerResource));
		assertNotNull(repository.save(floatResource));
		assertNotNull(repository.save(characterResource));
		assertNotNull(repository.save(booleanResource));
		assertEquals(2, repository.findAllByReadOnly(false).size());
		assertEquals(2, repository.findAllByReadOnly(true).size());
	}
}
