package dawid.brelak.greensunserver.repositories;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import dawid.brelak.greensunserver.model.analysis.Event;

/**
 * 
 * @author Dawid
 * @since 2019-01-19
 * @version 1
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class EventRepositoryTest {
	@Autowired
	private EventRepository eventRepository;
	
	@Test
	void repositoryTest(){		
		assertNotNull(eventRepository);
		assertTrue(eventRepository.checkAndSave(new Event(LocalDateTime.now().minusMinutes(10), "JUNIT", "event1", "Event Test 01")));
		assertFalse(eventRepository.checkAndSave(new Event(LocalDateTime.now(), "JUNIT", "event1", "Event Test 01")));	
		assertTrue(eventRepository.checkAndSave(new Event(LocalDateTime.now(), "JUNIT", "event2", "Event Test 02")));
		assertEquals(2, eventRepository.findAll().size());
		assertTrue(eventRepository.checkAndSave(new Event(LocalDateTime.now().plusHours(1), "JUNIT", "event1", "Event Test 01")));
		assertTrue(eventRepository.checkAndSave(new Event(LocalDateTime.now().plusHours(2), "JUNIT", "event1", "Event Test 01")));
		assertFalse(eventRepository.checkAndSave(new Event(LocalDateTime.now().plusMinutes(20), "JUNIT", "event2", "Event Test 02")));
		for (Event e  : eventRepository.findAll()) {
			System.err.println(e.toString());
		}
		assertEquals(4, eventRepository.findAll().size());		
	}

}
