package dawid.brelak.greensunserver.repositories;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;
import dawid.brelak.greensunserver.model.trigger.StandardTrigger;
import dawid.brelak.greensunserver.model.trigger.TriggerAction;

/**
 * 
 * @author Dawid
 * @since 2019-01-19
 * @version 1
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class TriggerRepositoryTest {
	@Autowired
	private TriggerRepository repository;
	@Autowired
	private RestDeviceRepository deviceRepository;

	@Test
	public void repositoryTest() {
		repository.deleteAll();
		RestDevice device = new RestDevice();
		device.setDescription("JUNIT TEST");
		device.setName("JUNIT DEVICE");
		device.setAddress("127.0.0.1");
		CharacterResource characterResource = new CharacterResource("Character", "Character", device, null, 6);
		BooleanResource booleanResource = new BooleanResource("Boolean", "Boolean", device, null, "ON", "OFF");
		IntegerResource integerResource = new IntegerResource("Integer", "Integer", device, null, 0, 100);
		FloatResource floatResource = new FloatResource("Float", "Float", device, null, 0f,100f);
		device.setResources(Arrays.asList(characterResource,booleanResource,integerResource,floatResource));
		StandardTrigger standardTrigger = new StandardTrigger();
		standardTrigger.setSourceResource(integerResource);
		standardTrigger.setSourceValue("50");
		standardTrigger.setTriggerType(StandardTrigger.TYPE_ABOVE_SINGLE);
		TriggerAction action1 = new TriggerAction();
		action1.setResource(floatResource);
		action1.setValue("30");
		TriggerAction action2 = new TriggerAction();
		action2.setResource(booleanResource);
		action2.setValue("ON");
		TriggerAction action3 = new TriggerAction();
		action3.setResource(characterResource);
		action3.setValue("JUNIT ACTION TEST");
		standardTrigger.getTriggerActions().add(action1);
		standardTrigger.getTriggerActions().add(action2);
		standardTrigger.getTriggerActions().add(action3);
		
		assertNotNull(repository);
		assertNotNull(deviceRepository);
		assertNotNull(deviceRepository.save(device));
		assertNotNull(repository.save(standardTrigger));		
		assertFalse(repository.findAll().isEmpty());
		assertEquals(standardTrigger, repository.findById(standardTrigger.getId()).get());
		assertNotNull(repository.findAllActions());
		assertEquals(3, repository.findAllActions().size());
		StandardTrigger readed = (StandardTrigger) repository.findById(standardTrigger.getId()).get();
		readed.getTriggerActions().remove(1);
		repository.removeAction(action2);
		assertEquals(2, repository.findById(standardTrigger.getId()).get().getTriggerActions().size());
		readed.getTriggerActions().clear();
		repository.removeActions(Arrays.asList(action1.getId(),action2.getId(),action3.getId()));
		assertEquals(0, repository.findById(standardTrigger.getId()).get().getTriggerActions().size());
		assertEquals(0, repository.findAllActions().size());
		repository.delete(standardTrigger);
		assertTrue(repository.findAll().isEmpty());
		repository.deleteAll();
	}
}
