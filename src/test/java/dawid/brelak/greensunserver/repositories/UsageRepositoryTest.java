package dawid.brelak.greensunserver.repositories;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsage;
import dawid.brelak.greensunserver.model.analysis.MonthlyResourceUsageId;
import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;

/**
 * 
 * @author Dawid
 * @since 2019-01-21
 * @version 1
 */
@ExtendWith(SpringExtension.class)
@DataJpaTest
public class UsageRepositoryTest {
	
	@Autowired
	private MontlyUsageRepository montlyUsageRepository;
	@Autowired
	private RestDeviceRepository deviceRepository;
	
	@Test
	void repositoryTest() {
		RestDevice device = new RestDevice();
		device.setDescription("JUNIT TEST");
		device.setName("JUNIT DEVICE");
		device.setAddress("127.0.0.1");
		CharacterResource characterResource = new CharacterResource("Character", "Character", device, null, 6);
		BooleanResource booleanResource = new BooleanResource("Boolean", "Boolean", device, null, "ON", "OFF");
		IntegerResource integerResource = new IntegerResource("Integer", "Integer", device, null, 0, 100);
		FloatResource floatResource = new FloatResource("Float", "Float", device, null, 0f,100f);
		device.setResources(Arrays.asList(characterResource,booleanResource,integerResource,floatResource));
		
		assertNotNull(montlyUsageRepository);
		assertNotNull(deviceRepository);
		assertFalse(montlyUsageRepository.removeByResourcesIds(Arrays.asList(1,2,3,4,5)));
		assertNotNull(deviceRepository.save(device));
		
		LocalDateTime time = LocalDateTime.now();
		MonthlyResourceUsage resourceUsage1 = new MonthlyResourceUsage();
		resourceUsage1.setDeviceResource(integerResource);
		resourceUsage1.setMonth(time.getMonthValue());
		resourceUsage1.setResourceId(integerResource.getId());
		resourceUsage1.setUsageCount(50l);
		resourceUsage1.setYear(time.getYear());
		time = LocalDateTime.now();
		MonthlyResourceUsage resourceUsage2 = new MonthlyResourceUsage();
		resourceUsage2.setDeviceResource(floatResource);
		resourceUsage2.setMonth(time.getMonthValue());
		resourceUsage2.setResourceId(floatResource.getId());
		resourceUsage2.setUsageCount(10l);
		resourceUsage2.setYear(time.getYear());
		time = LocalDateTime.now();
		MonthlyResourceUsage resourceUsage3 = new MonthlyResourceUsage();
		resourceUsage3.setDeviceResource(booleanResource);
		resourceUsage3.setMonth(time.getMonthValue());
		resourceUsage3.setResourceId(booleanResource.getId());
		resourceUsage3.setUsageCount(50l);
		resourceUsage3.setYear(time.getYear());
		
		assertNotNull(montlyUsageRepository.save(resourceUsage1));
		assertNotNull(montlyUsageRepository.save(resourceUsage2));
		assertNotNull(montlyUsageRepository.save(resourceUsage3));
		assertNotNull(montlyUsageRepository.getMonthlyKeys(time.getYear(), time.getMonthValue()));
		assertEquals(3,montlyUsageRepository.getMonthlyKeys(time.getYear(), time.getMonthValue()).size());
		assertNotNull(montlyUsageRepository.findAllByYearAndMonth(time.getYear(), time.getMonthValue()));
		assertEquals(3,montlyUsageRepository.findAllByYearAndMonth(time.getYear(), time.getMonthValue()).size());
		
		MonthlyResourceUsageId id1 = new MonthlyResourceUsageId(resourceUsage1.getYear(), resourceUsage1.getMonth(), resourceUsage1.getResourceId());
		MonthlyResourceUsageId id2 = new MonthlyResourceUsageId(resourceUsage2.getYear(), resourceUsage2.getMonth(), resourceUsage2.getResourceId());
		MonthlyResourceUsageId id3 = new MonthlyResourceUsageId(resourceUsage3.getYear(), resourceUsage3.getMonth(), resourceUsage3.getResourceId());
		assertNotNull(montlyUsageRepository.findAllByKeys(new HashSet<>(Arrays.asList(id1,id2,id3))));
		assertEquals(3,montlyUsageRepository.findAllByKeys(new HashSet<>(Arrays.asList(id1,id2,id3))).size());
		assertTrue(montlyUsageRepository.removeByResourcesIds(Arrays.asList(resourceUsage2.getResourceId())));
		assertNotNull(montlyUsageRepository.findAllByKeys(new HashSet<>(Arrays.asList(id1,id2,id3))));
		assertEquals(2,montlyUsageRepository.findAllByKeys(new HashSet<>(Arrays.asList(id1,id2,id3))).size());
	}

}
