package dawid.brelak.greensunserver.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import dawid.brelak.greensunserver.connectors.ARestConnector;
import dawid.brelak.greensunserver.model.arest.BooleanResponse;
import dawid.brelak.greensunserver.model.arest.CharacterResponse;
import dawid.brelak.greensunserver.model.arest.DeviceInfoResponse;
import dawid.brelak.greensunserver.model.arest.FloatResponse;
import dawid.brelak.greensunserver.model.arest.IntegerResponse;
import dawid.brelak.greensunserver.model.device.RestDevice;
import dawid.brelak.greensunserver.model.device.resources.BooleanResource;
import dawid.brelak.greensunserver.model.device.resources.CharacterResource;
import dawid.brelak.greensunserver.model.device.resources.FloatResource;
import dawid.brelak.greensunserver.model.device.resources.IntegerResource;
import dawid.brelak.greensunserver.repositories.EventRepository;
import dawid.brelak.greensunserver.repositories.RestDeviceRepository;

/**
 * 
 * @author Dawid
 * @since 2018-11-02
 * @version 2
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ARestServiceTest {
	@SpyBean
	ARestConnector aRestConnector;
	@MockBean
	EventRepository eventRepository;
	@Autowired
	ARestService aRestService;
	@Autowired
	private RestDeviceRepository deviceRepository;
	Random random = new Random();
	////
	RestDevice device = new RestDevice("Green sun developer ", "192.168.200.60",
			"Green sun developer module for connection template testing");
	BooleanResource booleanResource = new BooleanResource("boolean Resource", "boolean", device, null, "PowerON",
			"PowerOFF");
	BooleanResource brokenBooleanResource = new BooleanResource("boolean Resource", "boolean2", device, null, "PowerON",
			"PowerOFF");
	CharacterResource characterResource = new CharacterResource("character Resource", "character", device, null, 900);
	CharacterResource brokenCharacterResource = new CharacterResource("character Resource", "character2", device, null,
			900);
	IntegerResource integerResource = new IntegerResource("integer Resource", "integer", device, null, 0, 255);
	IntegerResource brokenIntegerResource = new IntegerResource("integer Resource 2", "integer2", device, null, 0, 255);
	FloatResource floatResource = new FloatResource("float Resource", "float", device, null, 0.0f, 255.0f);
	FloatResource brokenFloatResource = new FloatResource("float Resource", "float2", device, null, 0.0f, 255.0f);

	int iValue = 201;
	float fValue = 34.6f;
	String chValue = "Write Character Resource Test";

	@BeforeClass
	void onStart() {
		device.setResources(Arrays.asList(characterResource, booleanResource, integerResource, floatResource,
				brokenBooleanResource, brokenCharacterResource, brokenIntegerResource, brokenFloatResource));
		deviceRepository.save(device);
		when(eventRepository.checkAndSave(ArgumentMatchers.any())).thenReturn(true);
	}

	@BeforeEach
	void init() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("{\"" + floatResource.getResource() + "\": " + 86.65f + " ,");
		stringBuilder.append("\"id\": \"111\", ");
		stringBuilder.append("\"name\": " + device.getName() + ", ");
		stringBuilder.append(" \"hardware\": \"junit\", \"connected\": " + true + "}");
		when(aRestConnector.getAsString("http://" + device.getAddress() + "/" + floatResource.getResource()))
				.thenReturn(stringBuilder.toString());

		stringBuilder = new StringBuilder();
		stringBuilder.append("{\"" + integerResource.getResource() + "\": " + 60 + " ,");
		stringBuilder.append("\"id\": \"111\", ");
		stringBuilder.append("\"name\": " + device.getName() + ", ");
		stringBuilder.append(" \"hardware\": \"junit\", \"connected\": " + true + "}");
		when(aRestConnector.getAsString("http://" + device.getAddress() + "/" + integerResource.getResource()))
				.thenReturn(stringBuilder.toString());

		stringBuilder = new StringBuilder();
		stringBuilder.append("{\"" + characterResource.getResource() + "\": Kiedys to bylo,");
		stringBuilder.append("\"id\": \"111\", ");
		stringBuilder.append("\"name\": " + device.getName() + ", ");
		stringBuilder.append(" \"hardware\": \"junit\", \"connected\": " + true + "}");
		when(aRestConnector.getAsString("http://" + device.getAddress() + "/" + characterResource.getResource()))
				.thenReturn(stringBuilder.toString());

		when(aRestConnector.getAsString("http://" + device.getAddress() + "/" + booleanResource.getResource()))
				.thenReturn(getBooleanJSON());

		when(aRestConnector.getAsString("http://" + device.getAddress() + "/" + brokenIntegerResource.getResource()))
				.thenReturn(null);
		when(aRestConnector.getAsString("http://" + device.getAddress() + "/" + brokenFloatResource.getResource()))
				.thenReturn(null);
		when(aRestConnector.getAsString("http://" + device.getAddress() + "/" + brokenBooleanResource.getResource()))
				.thenReturn(null);
		when(aRestConnector.getAsString("http://" + device.getAddress() + "/" + brokenCharacterResource.getResource()))
				.thenReturn(null);

		stringBuilder = new StringBuilder();
		stringBuilder.append("{\"return_value\": 0, ");
		stringBuilder.append("\"id\": \"111\", ");
		stringBuilder.append("\"name\": \"" + device.getName() + "\", ");
		stringBuilder.append("\"hardware\": \"junit\", \"connected\": " + true + "}");
		when(aRestConnector.getAsString(
				"http://" + device.getAddress() + "/" + integerResource.getResource() + "Write?param=" + iValue))
						.thenReturn(stringBuilder.toString());
		when(aRestConnector.getAsString(
				"http://" + device.getAddress() + "/" + floatResource.getResource() + "Write?param=" + fValue))
						.thenReturn(stringBuilder.toString());
		when(aRestConnector.getAsString(
				"http://" + device.getAddress() + "/" + characterResource.getResource() + "Write?param=" + chValue))
						.thenReturn(stringBuilder.toString());
		when(aRestConnector
				.getAsString("http://" + device.getAddress() + "/" + booleanResource.getResource() + "Write?param=0"))
						.thenReturn(stringBuilder.toString());
		when(aRestConnector
				.getAsString("http://" + device.getAddress() + "/" + booleanResource.getResource() + "Write?param=1"))
						.thenReturn(stringBuilder.toString());
		DeviceInfoResponse deviceInfoResponse = new DeviceInfoResponse();
		deviceInfoResponse.setConnected(true);
		deviceInfoResponse.setHardware("junit");
		deviceInfoResponse.setName(device.getName());
		deviceInfoResponse.setId(deviceInfoResponse.getId());
		Map<String, Object> map = new HashMap<>();
		deviceInfoResponse.setVariables(map);
		when(aRestConnector.getAsObject("http://" + device.getAddress() + "/", DeviceInfoResponse.class))
				.thenReturn(deviceInfoResponse);
	}

	private String getBooleanJSON() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("{\"" + booleanResource.getResource() + "\": " + random.nextBoolean() + " ,");
		stringBuilder.append("\"id\": \"111\", ");
		stringBuilder.append("\"name\": " + device.getName() + ", ");
		stringBuilder.append(" \"hardware\": \"junit\", \"connected\": " + true + "}");
		return stringBuilder.toString();
	}

	@Test
	void getByResponseTypeTest() {

		assertNotNull(aRestService);
		FloatResponse floatResponse = (FloatResponse) aRestService.getByResponseType(
				"http://" + device.getAddress() + "/" + floatResource.getResource(), FloatResponse.class);
		assertNotNull(floatResponse);
		assertNotNull(floatResponse.getValueName());
		assertTrue(floatResponse.getValue() > 0);

		IntegerResponse integerResponse = (IntegerResponse) aRestService.getByResponseType(
				"http://" + device.getAddress() + "/" + integerResource.getResource(), IntegerResponse.class);
		assertNotNull(integerResponse);
		assertNotNull(integerResponse.getValueName());
		assertTrue(integerResponse.getValue() > 0);

		CharacterResponse characterResponse = (CharacterResponse) aRestService.getByResponseType(
				"http://" + device.getAddress() + "/" + characterResource.getResource(), CharacterResponse.class);
		assertNotNull(characterResponse);
		assertNotNull(characterResponse.getValueName());
		assertTrue(characterResponse.getValue().length() > 0);

		BooleanResponse booleanResponse = (BooleanResponse) aRestService.getByResponseType(
				"http://" + device.getAddress() + "/" + booleanResource.getResource(), BooleanResponse.class);
		assertNotNull(booleanResponse);
		assertNotNull(booleanResponse.getValueName());
		assertNotNull(booleanResponse.getValue());

		integerResponse = (IntegerResponse) aRestService.getByResponseType(
				"http://" + device.getAddress() + "/" + brokenIntegerResource.getResource(), IntegerResponse.class);
		assertNull(integerResponse);

		booleanResponse = (BooleanResponse) aRestService.getByResponseType(
				"http://" + device.getAddress() + "/" + brokenBooleanResource.getResource(), BooleanResponse.class);
		assertNull(booleanResponse);

		floatResponse = (FloatResponse) aRestService.getByResponseType(
				"http://" + device.getAddress() + "/" + brokenFloatResource.getResource(), FloatResponse.class);
		assertNull(floatResponse);

		characterResponse = (CharacterResponse) aRestService.getByResponseType(
				"http://" + device.getAddress() + "/" + brokenCharacterResource.getResource(), CharacterResponse.class);
		assertNull(characterResponse);
	}

	@Test
	void readResourceTest() {
		assertNotNull(aRestService);
		FloatResource fR = aRestService.readResource(floatResource);
		assertNotNull(fR.getCurrentState());
		IntegerResource iR = aRestService.readResource(integerResource);
		assertNotNull(iR.getCurrentState());
		CharacterResource cR = aRestService.readResource(characterResource);
		assertNotNull(cR.getCurrentState());
		BooleanResource bR = aRestService.readResource(booleanResource);
		assertNotNull(bR.getCurrentState());
		iR = aRestService.readResource(brokenIntegerResource);
		assertNotNull(iR);
		assertNull(iR.getCurrentState());
		fR = aRestService.readResource(brokenFloatResource);
		assertNotNull(fR);
		assertNull(fR.getCurrentState());
		cR = aRestService.readResource(brokenCharacterResource);
		assertNotNull(cR);
		assertNull(cR.getCurrentState());
		bR = aRestService.readResource(brokenBooleanResource);
		assertNotNull(bR);
		assertNull(bR.getCurrentState());
	}

	@Test
	void writeFloatResourceTest() {
		assertNotNull(aRestService);

		FloatResource fR = aRestService.writeResource(floatResource, -0.01f);
		assertNull(fR.getCurrentState());
		fR = aRestService.writeResource(floatResource, 255.01f);
		assertNull(fR.getCurrentState());
		fR = aRestService.writeResource(floatResource, null);
		assertNull(fR.getCurrentState());
		fR = aRestService.writeResource(floatResource, fValue);
		assertEquals(fValue, fR.getCurrentState());
	}

	@Test
	void writeIntegerResourceTest() {
		assertNotNull(aRestService);
		IntegerResource iR = aRestService.writeResource(integerResource, -1);
		assertNull(iR.getCurrentState());
		iR = aRestService.writeResource(integerResource, 256);
		assertNull(iR.getCurrentState());
		iR = aRestService.writeResource(integerResource, null);
		assertNull(iR.getCurrentState());
		iR = aRestService.writeResource(integerResource, iValue);
		assertEquals(iValue, iR.getCurrentState());
	}

	@Test
	void writeCharacterResourceTest() {
		assertNotNull(aRestService);
		CharacterResource cR = aRestService.writeResource(characterResource, "");
		assertNull(cR.getCurrentState());
		cR = aRestService.writeResource(characterResource, null);
		assertNull(cR.getCurrentState());
		cR = aRestService.writeResource(characterResource, chValue);
		assertEquals(chValue, cR.getCurrentState());
	}

	@Test
	void writeBooleanResourceTest() {
		assertNotNull(aRestService);
		String value = booleanResource.getOnLabel();
		BooleanResource bR = aRestService.writeResource(booleanResource, "Turbo");
		assertNull(bR.getCurrentState());
		bR = aRestService.writeResource(booleanResource, booleanResource.getOffLabel());
		assertNotNull(bR.getCurrentState());
		bR = aRestService.writeResource(booleanResource, value);
		assertEquals(value, bR.getCurrentState());

	}

	@Test
	void readDeviceTest() {
		assertNotNull(aRestService);
		RestDevice device2 = aRestService.readDevice(device);
		assertNotNull(device2);
		assertEquals(device2, device);

	}

}
